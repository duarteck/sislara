<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Click extends Model
{
    protected  $fillable = [
        'id',
        'post_id',
        'user_id',
      ];
    
    protected $table = 'clicks';
}
