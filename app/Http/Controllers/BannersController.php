<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Banner;

class BannersController extends Controller
{
	private $banner;

	public function __construct(){
		$this->middleware('auth');
		$this->banner = new Banner();
	}

	public function index($idDisc)
	{
		$list_banners = Banner::where('disciplina_id','=',$idDisc)->get();
		return view('banners.index', [
			'banners' => $list_banners,
			'disciplina' => $idDisc
		]);
	}

	public function novoView($idDisc)
	{
		return view('/banners.create', [
			'disciplina' => $idDisc
		]);
	}

	public function store(Request $request)
	{

		$disciplina_id = $request->input('disciplina_id');
		Banner::create($request->all());
		return redirect("/banners/$disciplina_id")->with("message", "Módulo Criada com Sucesso");
	}

	public function editarView($idDisc, $id)
	{
		return view('/banners.edit', [
			'banner' => $this->getBanner($id),
			'disciplina' => $idDisc
		]);
	}

	public function excluirView($idDisc, $id)
	{
		return view('/banners.delete', [
			'banner' => $this->getBanner($id),
			'disciplina' => $idDisc
		]);
	}

	public function destroy($idDisc, $id)
	{
		$banner = Banner::findOrFail($id);
		$banner->delete();
		return redirect("/banners/$idDisc")->with("message", "Módulo Excluida com Sucesso");
	}

	public function update(Request $request)
	{
		$disciplina_id = $request->input('disciplina_id');
		$banner = $this->getBanner($request->id);
                          $banner->update($request->all());
		return redirect("/banners/$disciplina_id")->with("message", "Módulo Atualizada com Sucesso");
	}

	protected function getBanner($id)
	{
		return  $this->banner->find($id);
	}

}
