<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Disciplina;

class PreviewController extends Controller
{

    private $disciplina;

	public function __construct(){
		$this->middleware('auth');
		$this->disciplina = new Disciplina();
	}

	public function index($id)
	{
		return view('/preview.index', [
			'disciplina' => $this->getDisciplina($id),

		]);
	}

	protected function getDisciplina($id)
	{
		return  $this->disciplina->find($id);
	}
}
