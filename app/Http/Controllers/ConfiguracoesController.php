<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Configuracoes;
use Auth;

class ConfiguracoesController extends Controller
{

    private $configuracao;

    public function __construct()
    {
        $this->middleware('auth');
        $this->configuracao = new Configuracoes();
    }

    public function index($id)
    {
        $view_configuracoes = Configuracoes::where('user_id','=',$id)->get();
        return view('configuracoes.index', [
            'configuracoes' => $view_configuracoes
        ]);
    }

    public function store(Request $request)
    {
        if ($request->id != null) {
            $configuracao = $this->getConfiguracoes($request->id);
            $configuracao->update($request->all());
        } else {
            $idUser = Auth::user()->id;
            $request['user_id'] = $idUser;
            Configuracoes::create($request->all());
        }
        return back()->with("message", "Dados salvos com Sucesso");
    }

    protected function getConfiguracoes($id)
    {
        return $this->configuracao->find($id);
    }
}
