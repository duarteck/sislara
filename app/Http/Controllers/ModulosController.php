<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modulo;

class ModulosController extends Controller
{

	private $modulo;

	public function __construct(){
		$this->middleware('auth');
		$this->modulo = new Modulo();
	}

	public function index($idDisc)
	{
		$list_modulos = Modulo::where('disciplina_id','=',$idDisc)->get();
		return view('modulos.index', [
			'modulos' => $list_modulos,
			'disciplina' => $idDisc
		]);
	}

	public function novoView($idDisc)
	{
		return view('/modulos.create', [
			'disciplina' => $idDisc
		]);
	}

	public function store(Request $request)
	{

		$disciplina_id = $request->input('disciplina_id');
		Modulo::create($request->all());
		return redirect("/modulos/$disciplina_id")->with("message", "Módulo Criada com Sucesso");
	}

	public function editarView($idDisc, $id)
	{
		return view('/modulos.edit', [
			'modulo' => $this->getModulo($id),
			'disciplina' => $idDisc
		]);
	}

	public function excluirView($idDisc, $id)
	{
		return view('/modulos.delete', [
			'modulo' => $this->getModulo($id),
			'disciplina' => $idDisc
		]);
	}

	public function destroy($idDisc, $id)
	{
		$modulo = Modulo::findOrFail($id);
		$modulo->delete();
		return redirect("/modulos/$idDisc")->with("message", "Módulo Excluida com Sucesso");
	}

	public function update(Request $request)
	{
		$disciplina_id = $request->input('disciplina_id');
		$modulo = $this->getModulo($request->id);
                          $modulo->update($request->all());
		return redirect("/modulos/$disciplina_id")->with("message", "Módulo Atualizada com Sucesso");
	}

	protected function getModulo($id)
	{
		return  $this->modulo->find($id);
	}

}
