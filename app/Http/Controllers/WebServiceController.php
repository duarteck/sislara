<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Configuracoes;
use App\Disciplina;
use Auth;

class WebServiceController extends Controller
{
    
    
    public function __construct(){
        
    }
    
    public function index($tokenDisc)
    {
        $disciplina = Disciplina::where('token',$tokenDisc)->first();
        
        if(empty($disciplina)){
            return "Disciplina não encontrada.";
        }

        $view_configuracoes = Configuracoes::where('user_id','=',$disciplina->user_id)->get();
        
        $view_disciplina = Disciplina::where('id','=',$disciplina->id)->get();
        
        $course_find = new Client([
            // Base URI is used with relative requests
            'base_uri' => 'http://'.$view_configuracoes[0]->url.'/webservice/rest/server.php?wstoken='.$view_configuracoes[0]->token.'&wsfunction=core_course_get_courses&moodlewsrestformat=json&options[ids][0]='.$disciplina->cursoid,
            // You can set any number of default request options.
            'timeout' => 20.0
        ]);
        
        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => 'http://'.$view_configuracoes[0]->url.'/webservice/rest/server.php?wstoken='.$view_configuracoes[0]->token.'&wsfunction=core_course_get_contents&courseid='.$disciplina->cursoid.'&moodlewsrestformat=json',
            // You can set any number of default request options.
            'timeout' => 20.0
        ]);
        
        $response_course_find = $course_find->request('GET', '');
        $response = $client->request('GET', '');
        
        $coursers = json_decode($response->getBody()->getContents());
        $course_find = json_decode($response_course_find->getBody()->getContents());

        return view('coursers.coursers2', compact('coursers', 'course_find', 'view_disciplina', 'view_configuracoes')); 
        
    }
    
}
