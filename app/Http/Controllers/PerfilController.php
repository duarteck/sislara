<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Illuminate\Support\Facades\Validator;
use Hash;

class PerfilController extends Controller
{
    private $user;

    public function __construct(){
        $this->middleware('auth');
        $this->user = new User();
    }
    
    public function index(){
        $user = Auth::user();
        // dd($user);
        return view('perfil.index',compact('user'));
    }

    public function update(Request $req){
        $user = $this->user->find($req->id);
        $user->update($req->all());

        return back()->with('message', 'Dados atualizados com sucesso.');
    }

    public function updateSenha(Request $req){

        Validator::make($req->all(), [
            'password' => 'required|string|min:6|confirmed',
            ],
            [
            'required' => 'O campo :attribute é obrigatório',
            'min' => 'A :attribute deve conter um mínimo de 6 caracteres.',
            'confirmed' => 'A confirmação de :attribute está incorreta.',
            ], 
            [
            'password'  => 'Senha',
            ]
        )->validate();
        
        $req['password'] = Hash::make($req['password']);

        $this->user->find($req->id)->update($req->all());

        return back()->with('message','Senha alterada com sucesso.');

    }
}
