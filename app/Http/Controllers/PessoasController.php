<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pessoa;

class PessoasController extends Controller
{



  public function index()
  {
    $list_pessoas = Pessoa::all();
    return view('pessoas.index', [
      'pessoas' => $list_pessoas
    ]);
  }

  public function novoView()
  {
    return view('pessoas.create');
  }

  public function store(Request $request)
  {
    Pessoa::create($request->all());
    return redirect('/pessoas')->with("message", "Pessoa Criada com Sucesso");
  }


}
