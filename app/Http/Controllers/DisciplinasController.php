<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Disciplina;
use Auth;
use GuzzleHttp\Client;
use App\Configuracoes;
use Hash;
use File;

class DisciplinasController extends Controller
{

	private $disciplina;

	public function __construct(){
		$this->middleware('auth');
		$this->disciplina = new Disciplina();
	}

	public function index()
	{
		$idUser = Auth::user()->id;

		$list_disciplinas = Disciplina::where('user_id','=',$idUser)->get();
		return view('disciplinas.index', [
			'disciplinas' => $list_disciplinas
		]);
	}

	public function novoView()
	{	
		$config = Configuracoes::where('user_id',Auth::user()->id)->first();

		$courses = [];

		if(!empty($config)){
			
			$url  = 'http://'.$config->url.'/webservice/rest/server.php?wstoken='.$config->token.'&wsfunction=core_course_get_courses&moodlewsrestformat=json';
			$curl = curl_init($url);
			curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,false);
			curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
			
			$retorno = curl_exec($curl);
			
			curl_close($curl);

			$courses = json_decode($retorno);
		}

		return view('disciplinas.create',compact('courses'));
	}

	public function store(Request $request)
	{

        $idUser = Auth::user()->id;
        
        $view_configuracoes = Configuracoes::where('user_id','=',$idUser)->get();

        if(empty($view_configuracoes[0]->token) || empty($view_configuracoes[0]->url)){
        	return back()->with('message','Ausencia de Token ou URL');
        }
		
		if($request->cursoid == 0){
			return back()->with('message','Atenção! Selecione um curso.');
		}

        $course_find = new Client([
            // Base URI is used with relative requests
            'base_uri' => 'http://'.$view_configuracoes[0]->url.'/webservice/rest/server.php?wstoken='.$view_configuracoes[0]->token.'&wsfunction=core_course_get_courses&moodlewsrestformat=json&options[ids][0]='.$request->cursoid,
            // You can set any number of default request options.
            'timeout' => 4.0
        ]);

        if($request->hasFile('image')){
       
	       	// Define um aleatório para o arquivo baseado no timestamps atual
	       	$name = uniqid(date('HisYmd'));
	 
	        // Recupera a extensão do arquivo
	        $extension = $request->image->extension();
	 
	        // Define finalmente o nome
	        $nameFile = "{$name}.{$extension}";
			
			// Faz o upload:
        	$upload = $request->image->storeAs('public/topos', $nameFile);
		}else{
			$nameFile = "image_topo_padrao.jpg";
		}

		$hash = str_replace('/', '0', hash('ripemd160', date('Y-m-d H:i:s')));

        $response_course_find = $course_find->request('GET', '');
        $course_find = json_decode($response_course_find->getBody()->getContents());
        $request['topo'] = $nameFile;
        $request['nome'] = $course_find[0]->fullname;
        $request['dtIn'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['dtIn'])));
        $request['dtFm'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['dtFm'])));
        $request['token'] = $hash;
		
		Disciplina::create($request->all());
		
		return redirect('/disciplinas')->with("message", "Disciplina Criada com Sucesso!");
	}

	public function editarView($id)
	{
		return view('/disciplinas.edit', [
			'disciplina' => $this->getDisciplina($id)
		]);
	}

	public function excluirView($id)
	{
		return view('/disciplinas.delete', [
			'disciplina' => $this->getDisciplina($id)
		]);
	}

	public function destroy($id)
	{
		$this->getDisciplina($id)->delete();
		return redirect('/disciplinas')->with("message", "Disciplina Excluída com Sucesso!");
	}

	public function update(Request $request)
	{
	    
	    $idUser = Auth::user()->id;
        
        $view_configuracoes = Configuracoes::where('user_id','=',$idUser)->get();
	    //$view_configuracoes = Configuracoes::all();
	    $course_find = new Client([
	        // Base URI is used with relative requests
	        'base_uri' => 'http://'.$view_configuracoes[0]->url.'/webservice/rest/server.php?wstoken='.$view_configuracoes[0]->token.'&wsfunction=core_course_get_courses&moodlewsrestformat=json&options[ids][0]='.$request->cursoid,
	        // You can set any number of default request options.
	        'timeout' => 4.0
	    ]);
	    
	    $response_course_find = $course_find->request('GET', '');
	    $course_find = json_decode($response_course_find->getBody()->getContents());
	    $request['nome'] = $course_find[0]->fullname;
	    $request['dtIn'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['dtIn'])));
        $request['dtFm'] = date('Y-m-d',strtotime(str_replace('/', '-', $request['dtFm'])));

		$disciplina = $this->getDisciplina($request->id);
		$disciplina->update($request->all());
		return redirect('/disciplinas')->with("message", "Disciplina Atualizada com Sucesso!");
	}

	protected function getDisciplina($id)
	{
		return  $this->disciplina->find($id);
	}


}
