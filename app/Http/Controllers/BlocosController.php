<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bloco;

class BlocosController extends Controller
{

private $bloco;

    public function __construct(){
        $this->bloco = new Bloco();
    }

    public function index($idMod)
    {
      //  var_dump($idMod);
        $list_blocos = Bloco::where('modulo_id','=',$idMod)->get();
        return view('blocos.index', [
            'blocos' => $list_blocos,
            'modulo' => $idMod
        ]);
    }

    public function novoView($idMod)
    {
        return view('/blocos.create', [
            'modulo' => $idMod
        ]);
    }

   public function store(Request $request)
    {
        $modulo_id = $request->input('modulo_id');
        Bloco::create($request->all());
        return redirect("/blocos/$modulo_id")->with("message", "Bloco Criado com Sucesso");
    }

    public function editarView($idMod, $id)
    {
        return view('/blocos.edit', [
            'bloco' => $this->getBloco($id),
            'modulo' => $idMod
        ]);
    }

    public function excluirView($idMod, $id)
    {
        return view('/blocos.delete', [
            'bloco' => $this->getBloco($id),
            'modulo' => $idMod
        ]);
    }

    public function destroy($idMod, $id)
    {
        $bloco = Bloco::findOrFail($id);
        $bloco->delete();
        return redirect("/blocos/$idMod")->with("message", "Bloco Excluído com Sucesso");
    }

    public function update(Request $request)
    {
        $modulo_id = $request->input('modulo_id');
        $bloco = $this->getBloco($request->id);
        $bloco->update($request->all());
        return redirect("/blocos/$modulo_id")->with("message", "Bloco Atualizado com Sucesso");
    }

protected function getBloco($id)
    {
        return  $this->bloco->find($id);
    }

}
