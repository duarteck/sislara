<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Item;

class ItensController extends Controller
{
    private $item;

    public function __construct(){
        $this->middleware('auth');
        $this->item = new Item();
    }

    public function index($idBlc)
    {
      //  var_dump($idMod);
        $list_itens = Item::where('bloco_id','=',$idBlc)->get();
        return view('itens.index', [
            'itens' => $list_itens,
            'bloco' => $idBlc
        ]);
    }

    public function novoView($idBlc)
    {
        return view('/itens.create', [
            'bloco' => $idBlc
        ]);
    }

 public function store(Request $request)
    {
        $bloco_id = $request->input('bloco_id');
        Item::create($request->all());
        return redirect("/itens/$bloco_id")->with("message", "Item Criado com Sucesso");
    }

       public function editarView($idBlc, $id)
    {
        return view('/itens.edit', [
            'item' => $this->getItem($id),
            'bloco' => $idBlc
        ]);
    }

     public function excluirView($idBlc, $id)
    {
        return view('/itens.delete', [
            'item' => $this->getItem($id),
            'bloco' => $idBlc
        ]);
    }

    public function destroy($idBlc, $id)
    {
        $item = Item::findOrFail($id);
        $item->delete();
        return redirect("/itens/$idBlc")->with("message", "Item Excluído com Sucesso");
    }

    public function update(Request $request)
    {
        $bloco_id = $request->input('bloco_id');
        $item = $this->getItem($request->id);
        $item->update($request->all());
        return redirect("/itens/$bloco_id")->with("message", "Item Atualizado com Sucesso");
    }

    protected function getItem($id)
    {
        return  $this->item->find($id);
    }

}
