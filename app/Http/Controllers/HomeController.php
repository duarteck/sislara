<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $perfil = Auth::user()->perfil;

        switch($perfil){
            case 1:
                return redirect('/admin');
                break;
            case 2:
                return redirect('/disciplinas');
                break;
        }
    }
}
