<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected  $fillable = [
    'id',
    'saudacao',
    'fasciculo',
    'video',
    'planos',
    'participantes',
    'notas',
    'disciplina_id'
  ];

    protected $table = 'banners';


    public function disciplina()
  {
    return $this->belongsTo(Disciplina::class, 'disciplina_id');
  }
}
