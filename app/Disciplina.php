<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Disciplina extends Model
{
  protected  $fillable = [
    'id',
    'nome',
    'professor',
    'cursoid',
    'dtIn',
    'dtFm',
    'sts',
    'topo',
    'user_id',
    'token'
  ];

  protected $table = 'disciplinas';

  public function user()
  {
    return $this->belongsTo(User::class, 'user_id');
  }

  public function modulos()
  {
    return $this->hasMany(Modulo::class, 'disciplina_id');
  }

  public function banners()
  {
    return $this->hasMany(Banner::class, 'disciplina_id');
  }

}
