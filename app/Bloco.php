<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bloco extends Model
{
    protected  $fillable = [
		'id',
		'nome',
		'icone',
		'sts',
		'modulo_id'
	];

	protected $table = 'blocos';

	public function modulo()
	{
		return $this->belongsTo(Modulo::class, 'modulo_id');
	}

	public function itens()
	{
		return $this->hasMany(Item::class, 'bloco_id');
	}
}
