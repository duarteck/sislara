<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Configuracoes extends Model
{

 protected  $fillable = [
    'id',
    'token',
    'url',
    'user_id'
  ];

 protected $table = 'configuracoes';
 
 public function user()
 {
     return $this->belongsTo(User::class, 'user_id');
 }

}
