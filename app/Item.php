<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected  $fillable = [
        'id',
        'nome',
        'link',
        'sts',
        'icone',
        'bloco_id'
    ];

    protected $table = 'itens';

    public function bloco()
    {
        return $this->belongsTo(Bloco::class, 'bloco_id');
    }
}
