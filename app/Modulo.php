<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modulo extends Model
{
	protected  $fillable = [
		'id',
		'nome',
		'cor',
		'visualizacao',
		'sts',
		'disciplina_id'
	];

	protected $table = 'modulos';

	public function disciplina()
	{
		return $this->belongsTo(Disciplina::class, 'disciplina_id');
	}

	public function blocos()
	{
		return $this->hasMany(Bloco::class, 'modulo_id');
	}
}
