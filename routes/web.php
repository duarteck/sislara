<?php

use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\User;

Auth::routes();

Route::get('/teste', function () {
    return view('teste');
});

Route::group([
    "prefix" => "admin"
],function(){
    Route::get("/","AdminController@index");
});

Route::get('/', function(){
    $findAdmin = User::where('email', 'admin@sislara.tk')->first();

    if(empty($findAdmin)){
        DB::table('users')->insert([
            'name' => 'admin', 
            'email' => 'admin@sislara.tk', 
            'password' => Hash::make('123456'),
            'perfil' => 1,
            ]);
    }

    return redirect('/home');
});

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('tasks', 'TasksController');

Route::group([
    "prefix" => "configuracoes"
], function () {
    Route::get("/{id}", "ConfiguracoesController@index");
    Route::post("/store", "ConfiguracoesController@store");
});

Route::group([
    "prefix" => "perfil"
], function () {
    Route::get("/", "PerfilController@index");
    Route::post("/update", "PerfilController@update");
    Route::get("/senha",function(){
        return view('perfil.senha');
    });
    Route::post("/senha/update", "PerfilController@updateSenha");
});

Route::group([
    "prefix" => "pessoas"
], function () {
    Route::get("/", "PessoasController@index");
    Route::get("/novo", "PessoasController@novoView");
    Route::post("/store", "PessoasController@store");
});

Route::group([
    "prefix" => "disciplinas"
], function () {
    Route::get("/", "DisciplinasController@index");
    Route::get("/novo", "DisciplinasController@novoView");
    Route::get("/{id}/editar", "DisciplinasController@editarView");
    Route::get("/{id}/excluir", "DisciplinasController@excluirView");
    Route::get("/{id}/destroy", "DisciplinasController@destroy");
    Route::post("/store", "DisciplinasController@store");
    Route::post("/update", "DisciplinasController@update");
});

Route::group([
    "prefix" => "modulos"
], function () {
    Route::get("/{idDisc}", "ModulosController@index");
    Route::get("/{idDisc}/novo", "ModulosController@novoView");
    Route::get("/{idDisc}/editar/{id}", "ModulosController@editarView");
    Route::get("/{idDisc}/excluir/{id}", "ModulosController@excluirView");
    Route::get("/{idDisc}/destroy/{id}", "ModulosController@destroy");
    Route::post("/store", "ModulosController@store");
    Route::post("/update", "ModulosController@update");
});

Route::group([
    "prefix" => "blocos"
], function () {
    Route::get("/{idMod}", "BlocosController@index");
    Route::get("/{idMod}/novo", "BlocosController@novoView");
    Route::get("/{idMod}/editar/{id}", "BlocosController@editarView");
    Route::get("/{idMod}/excluir/{id}", "BlocosController@excluirView");
    Route::get("/{idMod}/destroy/{id}", "BlocosController@destroy");
    Route::post("/store", "BlocosController@store");
    Route::post("/update", "BlocosController@update");
});

Route::group([
    "prefix" => "itens"
], function () {
    Route::get("/{idBlc}", "ItensController@index");
    Route::get("/{idBlc}/novo", "ItensController@novoView");
    Route::get("/{idBlc}/editar/{id}", "ItensController@editarView");
    Route::get("/{idBlc}/excluir/{id}", "ItensController@excluirView");
    Route::get("/{idBlc}/destroy/{id}", "ItensController@destroy");
    Route::post("/store", "ItensController@store");
    Route::post("/update", "ItensController@update");
});

Route::group([
    "prefix" => "banners"
], function () {
    Route::get("/{idDisc}", "BannersController@index");
    Route::get("/{idDisc}/novo", "BannersController@novoView");
    Route::get("/{idDisc}/editar/{id}", "BannersController@editarView");
    Route::get("/{idDisc}/excluir/{id}", "BannersController@excluirView");
    Route::get("/{idDisc}/destroy/{id}", "BannersController@destroy");
    Route::post("/store", "BannersController@store");
    Route::post("/update", "BannersController@update");
});

Route::group([
    "prefix" => "preview"
], function () {
    Route::get("/{id}", "PreviewController@index");
});

Route::get('/messages', 'PostController@myform');
Route::post('/messages', 'PostController@myformPost');

Route::get('/salvar/{idPost}/{idUser}', 'PostController@clickPost');

// Route::post('/customer/ajaxupdate', 'PostController@updateCustomerRecord');

Route::post('geo-info-response', 'PostController@updateCustomerRecord');

Route::get('/wsuser', function () {
    
    $client = new Client([
        // Base URI is used with relative requests
        'base_uri' => 'http://moodle.eadbooks.com.br/webservice/rest/server.php?wstoken=dc3554109fa2890d540b370b769cda56&wsfunction=core_enrol_get_enrolled_users&courseid=1&moodlewsrestformat=json',
        // You can set any number of default request options.
        'timeout' => 2.0
    ]);
    
    $response = $client->request('GET', '');
    
    $users = json_decode($response->getBody()->getContents());
    return view('users.users', compact('users'));
});

Route::get('/wscourse2', function () {
    
    $course_find = new Client([
        // Base URI is used with relative requests
        'base_uri' => 'http://moodle.eadbooks.com.br/webservice/rest/server.php?wstoken=dc3554109fa2890d540b370b769cda56&wsfunction=core_course_get_courses&moodlewsrestformat=json&options[ids][0]=11',
        // You can set any number of default request options.
        'timeout' => 2.0
    ]);
    
    $client = new Client([
        // Base URI is used with relative requests
        'base_uri' => 'http://moodle.eadbooks.com.br/webservice/rest/server.php?wstoken=dc3554109fa2890d540b370b769cda56&wsfunction=core_course_get_contents&courseid=11&moodlewsrestformat=json',
        // You can set any number of default request options.
        'timeout' => 2.0
    ]);
    
    $response_course_find = $course_find->request('GET', '');
    $response = $client->request('GET', '');
    
    $coursers = json_decode($response->getBody()->getContents());
    $course_find = json_decode($response_course_find->getBody()->getContents());
    return view('coursers.coursers2', compact('coursers'), compact('course_find'));
});

Route::group([
    "prefix" => "wscourse"
], function () {
    Route::get("/{tokenDisc}", "WebServiceController@index");
});





