<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>{{ config('app.name', 'SEAV') }}</title>

<!-- Styles -->
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>

	<div class="container">
		<h1>Curso</h1>

		@foreach($coursers as $courser) @if($courser->name == "Geral") 
		
		
		@else
		<div class="panel panel-default">
			<div class="panel-heading panel-primary">{{$courser->name}}</div>
			<div class="panel-body">

				<div class="form-group col-xs-4">
					<div class="panel panel-success">
						<div class="panel-heading panel-primary">Tarefas</div>
						<div class="panel-body">
						@foreach($courser->modules as $module)

							@if($module->modplural == "Tarefas") 
							{{ $module->name }} <br>
							@else

							@endif 
						@endforeach
						</div>
					</div>
				</div>
				
				<div class="form-group col-xs-4">
					<div class="panel panel-success">
						<div class="panel-heading panel-primary">Fóruns</div>
						<div class="panel-body">
						@foreach($courser->modules as $module)

							@if($module->modplural == "Fóruns") 
							{{ $module->name }} <br>
							@else

							@endif 
						@endforeach
						</div>
					</div>
				</div>
				
				<div class="form-group col-xs-4">
					<div class="panel panel-success">
						<div class="panel-heading panel-primary">Arquivos</div>
						<div class="panel-body">
						@foreach($courser->modules as $module)
                            @if($module->modplural == "Arquivos") 
							{{ $module->name }} <br>
							@else

							@endif 
						@endforeach
						</div>
					</div>
				</div>

			</div>
		</div>
		@endif 
		@endforeach

	</div>
</body>
</html>