<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="csrf-token" content="{{ csrf_token() }}" />
<title>Sislara</title>

<!-- Styles -->
<link href="{{ asset('css/app.css') }}" rel="stylesheet">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />

<!-- Ionicons -->
<link
	href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"
	rel="stylesheet" type="text/css" />
	
<link href="{{asset('css/font-awesome.min.css')}}"
	rel="stylesheet" type="text/css" />

<!-- Theme style -->
<link href="{{asset('css/AdminLTE.css')}}"
	rel="stylesheet" type="text/css" />
@php $topo = $view_disciplina[0]->topo; @endphp
<style>
	.jvectormap-label {
    position: absolute;
    display: none;
    border: solid 1px #CDCDCD;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
    background: #292929;
    color: white;
    font-size: 10px!important;
    padding: 3px;
    z-index: 9999;
}

.jvectormap-zoomin, .jvectormap-zoomout {
    position: absolute;
    left: 10px;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
    background: #292929;
    padding: 5px;
    color: white;
    cursor: pointer;
    line-height: 10px;
    text-align: center;
    font-weight: bold;
}

.jvectormap-zoomin {
    top: 10px;
}

.jvectormap-zoomout {
    top: 35px;
}

body {
	background-color: #FFFFFF
}

.corpo_topo {
	height: 241px;
	width: 100%;
	display: inline-block;
	background: url({{asset("storage/topos/$topo")}});
}
.blocoE {
	color: #FFFFFF;
	float: left;
	display: inline-block;
	position: relative;
	z-index: 1;
	height: 106px;
	width: 275px;
}

.header_1 {
	color: #fff;
	font-weight: bold;
}

.header_2 {
	color: #fff;
}

h2 {
	margin: 0;
	padding: 0;
	margin-left: 10px;
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
	font-size: 20px;
	padding-top: 1px;
	padding-left: 1px;
	padding-bottom: 8px;
}

.blocoE_txt {
	margin-top: 15px;
}

.blocoD ul {
	border: #E8E8E9 1px solid;
	width: 97px;
	box-shadow: 2px 1px 10px #858585;
}

.blocoD ul li {
	background: #FFF;
	color: #575757;
	padding-top: 6px;
	padding-bottom: 6px;
	padding-left: 10px;
	cursor: pointer;
}

.blocoD ul li:hover {
	background: #f1561e;
	color: #fff;
}

.span_carret {
	display: inline-block;
	width: 21px;
	height: 18px;
	background: url(images/carret.png) no-repeat;
	margin-left: 65px;
	margin-bottom: -15px;
}

.drop_nav {
	display: inline-block;
	right: 0;
	position: absolute;
	margin-top: -15px;
	display: none;
	overflow: hidden;
}

.corpo_topo a {
	text-decoration: none;
	color: #fff;
}
</style>

<!--menu novo-->
<style>
.breadcrumb {
	padding: 0px;
	background: #aaa;
	list-style: none;
	overflow: hidden;
	margin-top: -5px;
}

.breadcrumb>li+li:before {
	padding: 0;
}

.breadcrumb li {
	float: left;
}

.breadcrumb li.active a {
	background: brown; /* fallback color */
	background: #aaa;
}

.breadcrumb li.completed a {
	background: brown; /* fallback color */
	background: #aaa;
}

.breadcrumb li.active a:after {
	border-left: 30px solid #aaa;
}

.breadcrumb li.completed a:after {
	border-left: 30px solid #aaa;
}

.breadcrumb li a {
	color: white;
	text-decoration: none;
	padding: 10px 0 10px 45px;
	position: relative;
	display: block;
	float: left;
}

.breadcrumb li a:after {
	content: " ";
	display: block;
	width: 0;
	height: 0;
	border-top: 50px solid transparent;
	/* Go big on the size, and let overflow hide */
	border-bottom: 50px solid transparent;
	border-left: 30px solid #aaa;
	position: absolute;
	top: 50%;
	margin-top: -50px;
	left: 100%;
	z-index: 2;
}

.breadcrumb li a:before {
	content: " ";
	display: block;
	width: 0;
	height: 0;
	border-top: 50px solid transparent;
	/* Go big on the size, and let overflow hide */
	border-bottom: 50px solid transparent;
	border-left: 30px solid white;
	position: absolute;
	top: 50%;
	margin-top: -50px;
	margin-left: 1px;
	left: 100%;
	z-index: 1;
}

.breadcrumb li:first-child a {
	padding-left: 15px;
}

.breadcrumb li a:hover {
	background: #aaa
}

.breadcrumb li a:hover:after {
	border-left-color: #aaa;
	!
	important;
}
</style>

<!--fim menu novo-->

</head>
<body class="skin-blue">

	<!--topo com as informações do curso como nome da disciplina e data-->
	<div class="corpo_topo">
		<div class="blocoE" style="margin-left: 70px; margin-top: 50px;">
			<div class="blocoE_txt">
				<h3>
					@foreach($course_find as $courser_data) 
					<span class="header_1">{{ $courser_data->fullname}}</span>
					@endforeach
				</h3>
				<h5 class="header_2">Prof. {{ $view_disciplina[0]->professor }}</h5>
				<h5 class="header_2">Período: {{ \Carbon\Carbon::parse($view_disciplina[0]->dtIn)->format('d/m/Y') }} a {{ \Carbon\Carbon::parse($view_disciplina[0]->dtFm)->format('d/m/Y') }}</h5>
			</div>
		</div>
		<div class="blocoD"></div>
	</div>


	<!--novo menu de orientaçao-->
	<div class="">
		<div class="row">
			<ul class="breadcrumb">
				<li class="completed"><a href="#"><i
						style="margin-left: 15px; font-size: 20px"
						class="ion-android-menu"></i></a></li>
				<!--<li class="active"><a href="http://ava.ifma.edu.br/" target="_blank"><i
						style="margin-right: 15px; font-size: 20px"
						class="ion-android-home"></i>Página Inicial</a></li>-->
				<li class="active"><a href="#" onclick="javascript:parent.location='http://agpenvirtual.ma.gov.br/moodle/calendar/view.php?course={{ $courser_data->id}}'"><i
						style="margin-right: 15px; font-size: 20px"
						class="ion-ios-calendar-outline"></i>Calendário</a></li>
				<li class="active"><a href="#"><i
						style="margin-right: 15px; font-size: 20px"
						class="ion-ios-navigate-outline"></i>Guia do Aluno</a></li>
			</ul>
		</div>
	</div>

	<!--entrada dos blocos superiores-->

	<!--Bloco de apresentacao-->
	<div class="row">
		<div class="col-lg-4 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-aqua">
				<div class="inner">
					<h3>Apresenta&ccedil;&atilde;o</h3>
					<p>Apresenta&ccedil;&atilde;o da disciplina</p>
				</div>
				<div class="icon">
					<i class="ion-ios-book-outline"></i>
				</div>
				<a href="#" class="small-box-footer" data-toggle="modal"
					data-target="#myModal"> Visualizar <i class="ion-log-in"></i>
				</a>
			</div>
		</div>
		<!-- ./col -->


		<!--Modal de Apresentação da Disciplina-->
		<!-- Modal -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header" style="background-color: #00ADD8">
						<!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
						<h4 class="modal-title" id="myModalLabel" style="color: white">
							<i class="ion-ios-book-outline"
								style="margin-right: 15px; font-size: 20px"></i>Apresentação da
							Disciplina
						</h4>
					</div>
					<div class="modal-body">
						<!--tabs de Apresentação da Disciplina-->
						<div>
							<!-- Nav tabs -->
							<ul class="nav nav-tabs" role="tablist">
								<li role="presentation" class="active"><a href="#home"
									aria-controls="home" role="tab" data-toggle="tab"><i
										class="ion-android-happy"
										style="margin-right: 15px; font-size: 20px"></i>Saudações</a></li>
								<!--<li role="presentation"><a href="#fasciculo"
									aria-controls="profile" role="tab" data-toggle="tab"><i
										class="ion-android-document"
										style="margin-right: 15px; font-size: 20px"></i>Fascículo da
										Disciplina</a></li>
								<li role="presentation"><a href="#profile"
									aria-controls="profile" role="tab" data-toggle="tab"><i
										class="ion-social-youtube"
										style="margin-right: 15px; font-size: 20px"></i>Vídeo de
										Apresentação</a></li>-->
							</ul>
							<!-- Tab panes -->
							<div class="tab-content">
								<div role="tabpanel" class="tab-pane active" id="home"
									style="text-align: justify">
									<br>
									<!--aqui entra o texto  motivacional na guia de saudações-->
									@foreach($course_find as $courser_data) 
					                  {{  str_replace('"','',$courser_data->summary) }}
					                @endforeach
								</div>

								<div role="tabpanel" class="tab-pane" id="fasciculo">
									<br>
									<!--aqui entra o link dos fascúclos na guia de fasciculos-->
									<p>Área do Fasciculo</p>
								</div>
								<div role="tabpanel" class="tab-pane" id="profile">
									<br>
									<!--aqui entra o link do vídeo  na guia de Vídeos-->
									<p>Área do Vídeo</p>
								</div>
							</div>
						</div>
						<!--fim tabs-->
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
					</div>
				</div>
			</div>
		</div>
		<!--fim  modal-->
		<!--fim bloco de apresentacao  -->



		<!--bloco dos planos de ensino  
		<div class="col-lg-3 col-xs-6">
			<!-- small box 
			<div class="small-box bg-yellow">
				<div class="inner">
					<h3>Ementa</h3>
					<p>Ementa da Disciplina</p>
				</div>
				<div class="icon">
					<i class="ion-ios-paper-outline"></i>
				</div>
				<a href="#" class="small-box-footer"> Visualizar <i
					class="ion-log-in"></i>
				</a>
			</div>
		</div>
		<!--fim dos blocos planos de ensino  -->


		<!--bloco de Participantes  -->
		<div class="col-lg-4 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-red">
				<div class="inner">
					<h3>Participantes</h3>
					<p>Participantes da Disciplina</p>
				</div>
				<div class="icon">
					<i class="ion-android-contacts"></i>
				</div>
				@foreach($course_find as $courser_data) 
				<a href="#blocos" onclick="javascript:parent.location='http://agpenvirtual.ma.gov.br/moodle/user/index.php?id={{ $courser_data->id}}'" class="small-box-footer"> Visualizar <i
					class="ion-log-in"></i>
				</a>
				@endforeach
			</div>
		</div>
		<!-- fim bloco de Participantes  -->


		<!--bloco de notas  -->
		<div class="col-lg-4 col-xs-6">
			<div class="small-box bg-orange">
				<div class="inner">
					<h3>Notas</h3>
					<p>Notas da Disciplina</p>
				</div>
				<div class="icon">
					<i class="ion-ios-calculator-outline"></i>
				</div>
				@foreach($course_find as $courser_data) 
			<a href="#blocos" onclick="javascript:parent.location='http://agpenvirtual.ma.gov.br/moodle/grade/report/index.php?id={{ $courser_data->id}}'" class="small-box-footer"> Visualizar <i
					class="ion-log-in"></i>
				</a>
				@endforeach
			</div>
		</div>
	</div>
	<!--fim bloco de notas  -->

	  
     @php  $bgs = array("default", "primary", "success", "danger", "warning", "default", "primary", "success", "danger", "warning")  @endphp
 	 @php $cor = 0 @endphp
 	
	 @foreach($coursers as $courser) 
	 @if($courser->name == "Geral") 
	
	 @else
        @php $cor++ @endphp
        @php $atividade = 0 @endphp
 	    @php $forum = 0 @endphp
 	    @php $material = 0 @endphp
    <div class="row">
		<div class="box-body">
				<div class="box-group" id="accordion">
					<div class="panel box box-{{ $bg = $bgs[$cor] }}">
						<div class="box-header">
							<h4 class="box-title" style="margin-left:10px;">
								<a data-toggle="collapse" data-parent="#accordion"
									href="#collapse{{ $courser->id }}"> <i
									class="ion-ios-paperplane"></i> {{ $courser->name }}
								</a>
							</h4>
						</div>

						<div id="collapse{{ $courser->id }}"
							class="panel-collapse collapse">
							<div class="box-body">
								<div class="row">



                                            @foreach($courser->modules as $module)
                                              @if($module->modplural == "Arquivos" || $module->modplural == "URLs" || $module->modplural == "Pastas" || $module->modplural == "Certificados Simples")
                                               @php $material++ @endphp
                                              @endif 
											@endforeach
                                    @if($material == 0)
                                    
                                    @else
                                   <!-- Primary box Material Didático -->
									<div class="col-md-4">
										<div class="box box-solid box-{{ $bg = $bgs[$cor] }}">
											<div class="box-header">
												<h3 class="box-title">
													<i class="ion-ios-box-outline"></i> Material Didático
												</h3>
												<div class="box-tools pull-right">
													<button class="btn btn-{{ $bg = $bgs[$cor] }} btn-sm"
														data-widget="collapse">
														<i class="ion-minus"></i>
													</button>
												</div>
											</div>
											<div class="box-body">
												@foreach($courser->modules as $module)

												@if($module->modplural == "Arquivos" || $module->modplural == "URLs" || $module->modplural == "Pastas" || $module->modplural == "Certificados Simples")
												<p>

                                                    <a href="{{ $module->url }}" target="_blank" id="button1"
														data-sample-id="{{ $module->id }}" onclick="myFunction()">
														<span class="ion-document-text"></span> 
														<strong>{{ $module->name }}</strong>
													</a>

												</p>
												@else 
												
												@endif 
												@endforeach
											</div>
										</div>
									</div>
									<!-- Fim Primary box Material Didático-->
                                    @endif

                                            @foreach($courser->modules as $module)
                                              @if($module->modplural == "Fóruns")
                                               @php $forum++ @endphp
                                              @endif 
											@endforeach
                                    @if($forum == 0)
                                    
                                    @else
                                    <!-- Primary box Fóruns -->
									<div class="col-md-4">
										<div class="box box-solid box-{{ $bg = $bgs[$cor] }}">
											<div class="box-header">
												<h3 class="box-title">
													<i class="ion-android-chat"></i> Fóruns
												</h3>
												<div class="box-tools pull-right">
													<button class="btn btn-{{ $bg = $bgs[$cor] }} btn-sm"
														data-widget="collapse">
														<i class="ion-minus"></i>
													</button>
												</div>
											</div>
											<div class="box-body">
												@foreach($courser->modules as $module)

												@if($module->modplural == "Fóruns")
												<p>
													<a href="{{ $module->url }}" target="_blank" id="button1"
														data-sample-id="{{ $module->id }}" onclick="myFunction()">
														<span class="ion-android-textsms"></span> 
														<strong>{{ $module->name }}</strong>
													</a>
												</p>
												@else 
												
												@endif 
												@endforeach
											</div>
										</div>
									</div>
									<!-- Fim Primary box Fóruns-->
                                    @endif



                                            @foreach($courser->modules as $module)
                                              @if($module->modplural == "Tarefas" || $module->modplural == "Questionários")
                                               @php $atividade++ @endphp
                                              @endif 
											@endforeach
                                        @if($atividade == 0)
                                    
                                        @else
										<!-- Primary box Atividades -->
										<div class="col-md-4">
											<div class="box box-solid box-{{ $bg = $bgs[$cor] }}">
												<div class="box-header">
													<h3 class="box-title">
														<i class="ion-android-checkbox-outline"></i> Atividades
													</h3>
													<div class="box-tools pull-right">
														<button class="btn btn-{{ $bg = $bgs[$cor] }} btn-sm"
															data-widget="collapse">
															<i class="ion-minus"></i>
														</button>
													</div>
												</div>
												<div class="box-body">
													@foreach($courser->modules as $module)

													@if($module->modplural == "Tarefas" || $module->modplural == "Questionários" )
													<p>
														<a href="{{ $module->url }}" target="_blank" id="button1"
															data-post-id="{{ $module->id }}" data-user-id="1" data-token-id="{{ csrf_token() }}">
															<span class="ion-android-create	"></span> 
															<strong>{{ $module->name }}</strong>
														</a>
													</p>
													@else 
													
													@endif 
													@endforeach
												</div>
											</div>
										 </div>
										<!-- Fim Primary box Atividades-->
                                        @endif
									
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
	       @endif 
		   @endforeach
		
  
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
		
		<!-- jQuery 2.0.2 -->
		<script src="{{ asset('js/1jquery.js') }}"></script>
		
		<!-- Bootstrap -->
		<script src="{{ asset('js/bootstrap.min.js') }}"></script>
				
		<!-- Sparkline -->
		{{-- <script
			src="http://eadbooks.tempsite.ws/hadron/seav/js/plugins/sparkline/jquery.sparkline.min.js"
			type="text/javascript"></script> --}}
		
		<!-- AdminLTE App -->
		<script src="{{asset('js/AdminLTE.js')}}"
			type="text/javascript"></script>
		
		<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
		<script
			src="{{asset('js/dashboard.js')}}"
			type="text/javascript"></script>
		
		<!-- AdminLTE for demo purposes -->
		<script src="{{asset('js/demo.js')}}"
			type="text/javascript"></script>

   
   <script type="text/javascript">
      $(document).ready(function() {
	    $("#button1").click(function(e){
	    	e.preventDefault();
	    	var post_id = $(this).data("post-id");
	    	var user_id = $(this).data("user-id");
	    	var _token  = $(this).data("token-id");

	    	alert(post_id);
	    	
	    	 $.ajax({
	            url: "/messages",
	            type:'POST',
	            data: {_token:_token, post_id:post_id, user_id:user_id},
	            success: function(data) {
	                if($.isEmptyObject(data.error)){
	                	alert(data.success);
	                }else{
	                	printErrorMsg(data.error);
	                }
	            }
	        });

          }); 

         function printErrorMsg (msg) {
			$(".print-error-msg").find("ul").html('');
			$(".print-error-msg").css('display','block');
			$.each( msg, function( key, value ) {
				$(".print-error-msg").find("ul").append('<li>'+value+'</li>');
			});
		}
	});
 </script>


</body>
</html>