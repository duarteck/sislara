<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>{{ config('app.name', 'SEAV') }}</title>

<!-- Styles -->
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>

	<div class="container">
		<h1>Usuários do Curso</h1>

		@foreach($users as $user)
		<div class="panel panel-default">
			<div class="panel-body">{{$user->firstname.' '.$user->lastname}}</div>
		</div>
		@endforeach

	</div>
</body>
</html>