<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'SEAV') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Ionicons -->
    <link href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Custom Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <link href='//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>

    <style>
    .breadcrumb {
        padding: 0px;
        background: #F8F8F8;
        list-style: none;
        overflow: hidden;
        margin-top: -15px;
        font-style: italic;
    }
    .breadcrumb>li+li:before {
        padding: 0;
    }
    .breadcrumb li {
        float: left;
    }
    .breadcrumb li.active a {
        background: brown;                   /* fallback color */
        background: #F8F8F8 ;
    }
    .breadcrumb li.completed a {
        background: brown;                   /* fallback color */
        background: #F8F8F8;
    }
    .breadcrumb li.active a:after {
        border-left: 25px solid #F8F8F8 ;
    }
    .breadcrumb li.completed a:after {
        border-left: 25px solid #F8F8F8;
    }

    .breadcrumb li a {
        color: #000000;
        text-decoration: none;
        padding: 10px 0 10px 45px;
        position: relative;
        display: block;
        float: left;
    }
    .breadcrumb li a:after {
        content: " ";
        display: block;
        width: 0;
        height: 0;
        border-top: 50px solid transparent;           /* Go big on the size, and let overflow hide */
        border-bottom: 50px solid transparent;
        border-left: 30px solid #00A65A;
        position: absolute;
        top: 50%;
        margin-top: -50px;
        left: 100%;
        z-index: 2;
    }
    .breadcrumb li a:before {
        content: " ";
        display: block;
        width: 0;
        height: 0;
        border-top: 50px solid transparent;           /* Go big on the size, and let overflow hide */
        border-bottom: 50px solid transparent;
        border-left: 30px solid white;
        position: absolute;
        top: 50%;
        margin-top: -50px;
        margin-left: 1px;
        left: 100%;
        z-index: 1;
    }
    .breadcrumb li:first-child a {
        padding-left: 15px;
    }
    .breadcrumb li a:hover { background:#F8F8F8 ; text-decoration: underline; }
    .breadcrumb li a:hover:after { border-left-color:#F8F8F8;   !important; }

</style>


</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    SEAV
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    &nbsp;
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @guest
                    <li><a href="{{ route('login') }}">Login</a></li>
                    <li><a href="{{ route('register') }}">Registrar</a></li>
                    @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu">
                            <li><a href="{{url("/configuracoes/".Auth::user()->id)}}">Configurações</a></li>
                            <li>
                                <a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                Sair
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                        
                    </ul>
                </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Registro</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label class="col-md-4 control-label">Pessoa</label>

                            <div class="col-md-6">
                                <input id="input-fisico" type="radio" class="" name="pessoa" value="Física" checked>
                                <label class="control-label" style="margin-right: 15px;">Física</label>
                                <input id="input-juridico" type="radio" class="" name="pessoa" value="Jurídica">
                                <label class="control-label">Jurídica</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label id="label-pessoa" class="col-md-4 control-label">CPF</label>

                            <div class="col-md-6">
                                <input id="cpf-cnpj" type="text" class="form-control" name="cpfcnpj" value="{{ old('cpfcnpj') }}">
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Nome</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label id="label-pessoa" class="col-md-4 control-label">Celular</label>

                            <div class="col-md-6">
                                <input id="celular" type="text" class="form-control" name="celular" value="{{ old('celular') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label id="label-pessoa" class="col-md-4 control-label">CEP</label>

                            <div class="col-md-6">
                                <input id="cep" type="text" class="form-control" name="cep" value="{{ old('cep') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label id="label-pessoa" class="col-md-4 control-label">Endereço</label>

                            <div class="col-md-6">
                                <input id="endereco" type="text" class="form-control" name="endereco" value="{{ old('endereco') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label id="label-pessoa" class="col-md-4 control-label">Número</label>

                            <div class="col-md-6">
                                <input id="numero" type="text" class="form-control" name="numero" value="{{ old('numero') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label id="label-pessoa" class="col-md-4 control-label">Complemento</label>

                            <div class="col-md-6">
                                <input id="complemento" type="text" class="form-control" name="complemento" value="{{ old('complemento') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label id="label-pessoa" class="col-md-4 control-label">Bairro</label>

                            <div class="col-md-6">
                                <input id="bairro" type="text" class="form-control" name="bairro" value="{{ old('bairro') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label id="label-pessoa" class="col-md-4 control-label">Cidade</label>

                            <div class="col-md-6">
                                <input id="cidade" type="text" class="form-control" name="cidade" value="{{ old('cidade') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label id="label-pessoa" class="col-md-4 control-label">Estado</label>

                            <div class="col-md-6">
                                <input id="uf" type="text" class="form-control" name="estado" value="{{ old('estado') }}">
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Endereço de E-Mail</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Senha</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required autocomplete="false">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirmar Senha</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="false">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Registrar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<script src="{{ asset('js/app.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script type="text/javascript" src="{{asset("js/jquery.mask.js")}}"></script>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
    $('#cpf-cnpj').mask('000.000.000-00');
    $('#cep').mask('00.000-000');
    $('#celular').mask('(00) 00000-0000');
    $('#input-juridico').click(function(){
        $('#cpf-cnpj').val('');
        $('#label-pessoa').html('CNPJ');
        $('#cpf-cnpj').mask('00.000.000/0000-00');
    });
    $('#input-fisico').click(function(){
        $('#cpf-cnpj').val('');
        $('#label-pessoa').html('CPF');
        $('#cpf-cnpj').mask('000.000.000-00');
    });

    function limpa_formulário_cep() {
        // Limpa valores do formulário de cep.
        $("#endereco").val("");
        $("#bairro").val("");
        $("#cidade").val("");
        $("#uf").val("");
    }

    $("#cep").blur(function() {

            //Nova variável "cep" somente com dígitos.
            var cep = $(this).val().replace(/\D/g, '');

            //Verifica se campo cep possui valor informado.
            if (cep != "") {

                //Expressão regular para validar o CEP.
                var validacep = /^[0-9]{8}$/;

                //Valida o formato do CEP.
                if(validacep.test(cep)) {

                    $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                        if (!("erro" in dados)) {
                            //Atualiza os campos com os valores da consulta.
                            $("#endereco").val(dados.logradouro);
                            $("#bairro").val(dados.bairro);
                            $("#cidade").val(dados.localidade);
                            $("#uf").val(dados.uf);
                        } //end if.
                        else {
                            document.getElementById("cep").value = "";
                            limpa_formulário_cep();
                        }
                    }); 
                } //end if.
                else {
                    document.getElementById("cep").value = "";
                    limpa_formulário_cep();
                }
            } //end if.
            else {
                document.getElementById("cep").value = "";
            }
        });

});
</script>
</body>
</html>