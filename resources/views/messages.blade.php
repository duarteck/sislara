<!DOCTYPE html>
<html>
<head>
	<title>Laravel Ajax Validation Example</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	
	
	
	<!-- Ionicons -->
<link
	href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"
	rel="stylesheet" type="text/css" />
	
<link href="http://eadbooks.com.br/hadron/seav/css/font-awesome.min.css"
	rel="stylesheet" type="text/css" />
	
<link
	href="http://eadbooks.com.br/hadron/seav/css/jvectormap/jquery-jvectormap-1.2.2.css"
	rel="stylesheet" type="text/css" />

<!-- Theme style -->
<link href="http://eadbooks.com.br/hadron/seav/css/AdminLTE.css"
	rel="stylesheet" type="text/css" />
	
	
	
</head>
<body>


<div class="container">
	<h2>Ajax Validation</h2>


	<div class="alert alert-danger print-error-msg" style="display:none">
        <ul></ul>
    </div>


	<form>
		{{ csrf_field() }}
		<div class="form-group">
			<label>First Name:</label>
			<input type="text" name="first_name" class="form-control" placeholder="First Name">
		</div>


		<div class="form-group">
			<label>Last Name:</label>
			<input type="text" name="last_name" class="form-control" placeholder="Last Name">
		</div>


		<div class="form-group">
			<strong>Email:</strong>
			<input type="text" name="email" class="form-control" placeholder="Email">
		</div>


		<div class="form-group">
			<strong>Address:</strong>
			<textarea class="form-control" name="address" placeholder="Address"></textarea>
		</div>


		<div class="form-group">
			<button class="btn btn-success btn-submit" data-sample-id="1">Submit</button>
		</div>
		
		
		
		<div class="form-group">
		<a href="http://globo.com" target="_blank" id="button1" data-post-id="1" data-user-id="1" data-token-id="{{ csrf_token() }}"> 
			 <strong>Teste</strong>
			 	</a>
		</div>							
					
	</form>	
	
</div>





<!-- jQuery 2.0.2 -->
		<script src="http://eadbooks.com.br/hadron/seav/js/1jquery.min.js"></script>
		
		<!-- Bootstrap -->
		<script src="http://eadbooks.com.br/hadron/seav/js/bootstrap.min.js"
			type="text/javascript"></script>
				
		<!-- Sparkline -->
		<script
			src="http://eadbooks.com.br/hadron/seav/js/plugins/sparkline/jquery.sparkline.min.js"
			type="text/javascript"></script>
		
		<!-- AdminLTE App -->
		<script src="http://eadbooks.com.br/hadron/seav/js/AdminLTE/app.js"
			type="text/javascript"></script>
		
	
		
	


<script type="text/javascript">
    $(document).ready(function() {
	    $("#button1").click(function(e){
	    	e.preventDefault();
	    	var post_id = $(this).data("post-id");
	    	var user_id = $(this).data("user-id");
	    	var _token = $(this).data("token-id");
	    	var first_name = $("input[name='first_name']").val();
	    	var last_name = $("input[name='last_name']").val();
	    	var email = $("input[name='email']").val();
	    	var address = $("textarea[name='address']").val();
	    	

	        $.ajax({
	            url: "/messages",
	            type:'POST',
	            data: {_token:_token, post_id:post_id, user_id:user_id},
	            success: function(data) {
	                if($.isEmptyObject(data.error)){
	                	alert(data.success);
	                }else{
	                	printErrorMsg(data.error);
	                }
	            }
	        });


	    }); 


	    function printErrorMsg (msg) {
			$(".print-error-msg").find("ul").html('');
			$(".print-error-msg").css('display','block');
			$.each( msg, function( key, value ) {
				$(".print-error-msg").find("ul").append('<li>'+value+'</li>');
			});
		}
	});


</script>


</body>
</html>