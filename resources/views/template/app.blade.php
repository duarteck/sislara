<!doctype html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">


<title>SEAV</title>

<!-- Ionicons AKKKII-->
<link
	href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"
	rel="stylesheet" type="text/css" />
<!-- bootstrap 3.0.2 -->
<link href="http://eadbooks.tempsite.ws/hadron/seav/css/bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">
<!-- Custom Fonts -->
<link
	href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
	rel='stylesheet' type='text/css'>
<link
	href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic'
	rel='stylesheet' type='text/css'>

<link href='//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css'
	rel='stylesheet' type='text/css'>

<style>
.breadcrumb {
	padding: 0px;
	background: #F8F8F8;
	list-style: none;
	overflow: hidden;
	margin-top: -15px;
	font-style: italic;
}

.breadcrumb>li+li:before {
	padding: 0;
}

.breadcrumb li {
	float: left;
}

.breadcrumb li.active a {
	background: brown; /* fallback color */
	background: #F8F8F8;
}

.breadcrumb li.completed a {
	background: brown; /* fallback color */
	background: #F8F8F8;
}

.breadcrumb li.active a:after {
	border-left: 25px solid #F8F8F8;
}

.breadcrumb li.completed a:after {
	border-left: 25px solid #F8F8F8;
}

.breadcrumb li a {
	color: #000000;
	text-decoration: none;
	padding: 10px 0 10px 45px;
	position: relative;
	display: block;
	float: left;
}

.breadcrumb li a:after {
	content: " ";
	display: block;
	width: 0;
	height: 0;
	border-top: 50px solid transparent;
	/* Go big on the size, and let overflow hide */
	border-bottom: 50px solid transparent;
	border-left: 30px solid #00A65A;
	position: absolute;
	top: 50%;
	margin-top: -50px;
	left: 100%;
	z-index: 2;
}

.breadcrumb li a:before {
	content: " ";
	display: block;
	width: 0;
	height: 0;
	border-top: 50px solid transparent;
	/* Go big on the size, and let overflow hide */
	border-bottom: 50px solid transparent;
	border-left: 30px solid white;
	position: absolute;
	top: 50%;
	margin-top: -50px;
	margin-left: 1px;
	left: 100%;
	z-index: 1;
}

.breadcrumb li:first-child a {
	padding-left: 15px;
}

.breadcrumb li a:hover {
	background: #F8F8F8;
	text-decoration: underline;
}

.breadcrumb li a:hover:after {
	border-left-color: #F8F8F8;
	!
	important;
}
</style>

</head>
<body>

	@if (session('status'))
	<div class="alert alert-success">{{ session('status') }}</div>
	@endif


	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
					aria-expanded="false">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">SEAV</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false">Menu <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="{{url("/disciplinas")}}">Disciplinas</a></li>
						</ul></li>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
	</nav>


	@yield('content')
	<!--<div class="container">

</div>-->



	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>

	<!-- jQuery 2.0.2 -->
	<script src="http://eadbooks.tempsite.ws/hadron/seav/js/1jquery.min.js"></script>
	<!-- jQuery UI 1.10.3 -->
	<script
		src="http://eadbooks.tempsite.ws/hadron/seav/js/jquery-ui-1.10.3.min.js"
		type="text/javascript"></script>
	<!-- Bootstrap -->
	<script src="http://eadbooks.tempsite.ws/hadron/seav/js/bootstrap.min.js"
		type="text/javascript"></script>


	<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" charset="utf-8">
  $(document).ready(function() {
    $('#example').DataTable();
});
</script>

</body>
</html>