@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <ul class="breadcrumb">
            <li class="completed"><a href="#" target="_blank"><i style="margin-left: 15px; font-size: 20px" class="ion-android-menu"></i></a></li>
            <li class="active"><a href="#" target="_blank"><i style="margin-right: 15px; font-size: 20px" class="ion-android-home"></i>Página Inicial</a></li>
            <li class="active"><a href="{{url("/disciplinas")}}"><i style="margin-right: 15px; font-size: 20px" class="ion-university"></i>Listagem de Disciplinas</a></li>
        </ul>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    Você está logado!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
