@extends("layouts.app")

@section("content")

<div class="row">
    <div class="col-md-12">
        <ul class="breadcrumb">
            <li class="completed"><a href="#" target="_blank"><i style="margin-left: 15px; font-size: 20px" class="ion-android-menu"></i></a></li>
            <li class="active"><a href="#" target="_blank"><i style="margin-right: 15px; font-size: 20px" class="ion-android-home"></i>Página Inicial</a></li>
            <li class="active"><a href="{{url("/disciplinas")}}"><i style="margin-right: 15px; font-size: 20px" class="ion-university"></i>Listagem de Disciplinas</a></li>
           <li class="active"><a href="#"><i style="margin-right: 15px; font-size: 20px" class="ion-ios-browsers-outline"></i>Listagem de Modulo</a></li>
          <li class="active"><a href="{{url("/blocos/$modulo")}}"><i style="margin-right: 15px; font-size: 20px" class="ion-cube"></i>Listagem de Blocos</a></li>
          </ul>
    </div>
</div>

<div id="page-wrapper">
  <div class="col-md-12">
    <div class="panel panel-default">
      <a href="{{url("/blocos/$modulo/novo")}}">
        <button type="submit" style="margin-top:5px; margin-right:5px;" class="btn btn-default btn-sm pull-right">
          <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
        </button>
      </a>
      <div class="panel-heading">
        Listagem de Blocos
      </div>
      <br><br>

      <div class="panel-body">

        <table id="example" class="table table-striped" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>#</th>
              <th>Nome do Bloco</th>
            </tr>
          </thead>
          <tbody>
           @foreach($blocos as $bloco)
           <tr>
            <td>{{$bloco->id}}</td>
            <td>{{$bloco->nome}}
             <div class="btn-group pull-right">
              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                <li><a href="{{url("/itens/$bloco->id")}}"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> Listar Itens</a></li>
                <li><a href="{{url("/blocos/$modulo/editar/$bloco->id")}}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Editar Bloco</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="{{url("/blocos/$modulo/excluir/$bloco->id")}}"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span> Excluir</a></li>
              </ul>
            </div>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>


  </div>
</div>
</div>
</div>
@endsection