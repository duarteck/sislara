@extends("layouts.app")

@section("content")


<div id="page-wrapper">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        Cadastrar Bloco
      </div>
      <br><br>

      <div class="panel-body">

        <form action="{{url('/blocos/store')}}" method="POST">
          {{csrf_field()}}
          <input type="hidden" name="modulo_id" value="{{ $modulo }}">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Nome do Bloco:</label>
                <input type="text" class="form-control" name="nome" placeholder="Bloco">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Ícone do Bloco:</label><br>
                        <label class="radio-inline">
                         <h1><input type="radio" name="icone" id="icone" value="ion-ios-box-outline"><i class="ion-ios-box-outline"></i></h1>
                        </label>
                        <label class="radio-inline">
                            <h1><input type="radio" name="icone" id="icone" value="ion-android-checkbox-outline"> <i class="ion-android-checkbox-outline"></i></h1>
                        </label>
                        <label class="radio-inline">
                            <h1><input type="radio" name="icone" id="icone" value="ion-android-chat"> <i class="ion-android-chat"></i></h1>
                        </label>
           </div>
         </div>
       </div>

     <div class="row">
      <div class="col-md-6">
        <div class="form-group">
         <label>Status:</label><br>
         <label class="radio-inline">
          <input type="radio" name="sts" value="A"> Ativo
        </label>
        <label class="radio-inline">
          <input type="radio" name="sts" value="I"> Inativo
        </label>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-6">
     <div class="form-group pull-right">
       <button type="submit" class="btn btn-success">Adicionar</button>
       <a href="{{url("/blocos/$modulo")}}"><button type="button" class="btn btn-danger">Cancelar</button> </a>
     </div>
   </div>
 </div>
</form>

</div>
</div>
</div>
</div>
@endsection