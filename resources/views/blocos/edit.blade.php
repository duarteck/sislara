@extends("layouts.app")

@section("content")


<div id="page-wrapper">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        Editar Módulo
      </div>
      <br><br>

      <div class="panel-body">

        <form action="{{url('/blocos/update')}}" method="POST">
          {{csrf_field()}}
          <input type="hidden" name="modulo_id" value="{{$modulo}}">
          <input type="hidden" name="id" value="{{ $bloco->id }}">

          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Nome da Modulo:</label>
                <input type="text" class="form-control" name="nome" value="{{$bloco->nome}}" placeholder="Disciplina">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">

 <label>Ícone do Bloco:</label><br>
                        <label class="radio-inline">
                         <h1><input type="radio" name="icone" id="icone" value="ion-ios-box-outline" {{ ($bloco->icone == 'ion-ios-box-outline') ? 'checked' : '' }}><i class="ion-ios-box-outline"></i></h1>
                        </label>
                        <label class="radio-inline">
                            <h1><input type="radio" name="icone" id="icone" value="ion-android-checkbox-outline" {{ ($bloco->icone == 'ion-android-checkbox-outline') ? 'checked' : '' }}> <i class="ion-android-checkbox-outline"></i></h1>
                        </label>
                        <label class="radio-inline">
                            <h1><input type="radio" name="icone" id="icone" value="ion-android-chat" {{ ($bloco->icone == 'ion-android-chat') ? 'checked' : '' }}> <i class="ion-android-chat"></i></h1>
                        </label>
  </div>
         </div>
       </div>

     <div class="row">
      <div class="col-md-6">
        <div class="form-group">
         <label>Status:</label><br>
         <label class="radio-inline">
          <input type="radio" name="sts" value="A" {{ ($bloco->sts == 'A') ? 'checked' : '' }}> Ativo
        </label>
        <label class="radio-inline">
          <input type="radio" name="sts" value="I" {{ ($bloco->sts == 'I') ? 'checked' : '' }}> Inativo
        </label>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-6">
     <div class="form-group pull-right">
       <button type="submit" class="btn btn-success">Editar</button>
       <a href="{{url("/blocos/$modulo")}}"><button type="button" class="btn btn-danger">Cancelar</button> </a>
     </div>
   </div>
 </div>
</form>

</div>
</div>
</div>
</div>
@endsection