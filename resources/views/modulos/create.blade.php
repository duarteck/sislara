@extends("layouts.app")

@section("content")


<div id="page-wrapper">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        Adicionar de Módulo
      </div>
      <br><br>

      <div class="panel-body">

        <form action="{{url('/modulos/store')}}" method="POST">
          {{csrf_field()}}
          <input type="hidden" name="disciplina_id" value="{{ $disciplina }}">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Nome da Modulo:</label>
                <input type="text" class="form-control" name="nome" placeholder="Bloco">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
               <label class="radio-inline">
                <input type="radio" name="cor" id="cor" value="primary"> <button type="submit" class="btn btn-primary" disabled="true"></button>
              </label>
              <label class="radio-inline">
               <input type="radio" name="cor" id="cor" value="success"> <button type="submit" class="btn btn-success" disabled="true"></button>
             </label>
             <label class="radio-inline">
               <input type="radio" name="cor" id="cor" value="danger"> <button type="submit" class="btn btn-danger" disabled="true"></button>
             </label>
             <label class="radio-inline">
               <input type="radio" name="cor" id="cor" value="warning"> <button type="submit" class="btn btn-warning" disabled="true"></button>
             </label>
           </div>
         </div>
       </div>
       <div class="row">
        <div class="col-md-6">
          <div class="form-group">
           <label>Visualização:</label><br>
           <label class="radio-inline">
             <input type="radio" name="visualizacao" id="visualizacao" value="A"> Aberto
           </label>
           <label class="radio-inline">
             <input type="radio" name="visualizacao" id="visualizacao" value="F"> Fechado
           </label>
         </div>
       </div>
     </div>
     <div class="row">
      <div class="col-md-6">
        <div class="form-group">
         <label>Status:</label><br>
         <label class="radio-inline">
          <input type="radio" name="sts" value="A"> Ativo
        </label>
        <label class="radio-inline">
          <input type="radio" name="sts" value="I"> Inativo
        </label>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-6">
     <div class="form-group pull-right">
       <button type="submit" class="btn btn-success">Adicionar</button>
       <a href="{{url("/modulos/$disciplina")}}"><button type="button" class="btn btn-danger">Cancelar</button> </a>
     </div>
   </div>
 </div>
</form>

</div>
</div>
</div>
</div>
@endsection