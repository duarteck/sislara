@extends("layouts.app")

@section("content")


<div id="page-wrapper">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        Editar Módulo
      </div>
      <br><br>

      <div class="panel-body">

        <form action="{{url('/modulos/update')}}" method="POST">
 {{csrf_field()}}
          <input type="hidden" name="disciplina_id" value="{{$disciplina}}">
          <input type="hidden" name="id" value="{{ $modulo->id }}">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Nome da Modulo:</label>
                <input type="text" class="form-control" name="nome" value="{{$modulo->nome}}" placeholder="Disciplina">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
               <label class="radio-inline">
                <input type="radio" name="cor" id="cor" value="primary" {{ ($modulo->cor == 'primary') ? 'checked' : '' }}> <button type="submit" class="btn btn-primary" disabled="true"></button>
              </label>
              <label class="radio-inline">
               <input type="radio" name="cor" id="cor" value="success" {{ ($modulo->cor == 'success') ? 'checked' : '' }}> <button type="submit" class="btn btn-success" disabled="true"></button>
             </label>
             <label class="radio-inline">
               <input type="radio" name="cor" id="cor" value="danger" {{ ($modulo->cor == 'danger') ? 'checked' : '' }}> <button type="submit" class="btn btn-danger" disabled="true"></button>
             </label>
             <label class="radio-inline">
               <input type="radio" name="cor" id="cor" value="warning" {{ ($modulo->cor == 'warning') ? 'checked' : '' }}> <button type="submit" class="btn btn-warning" disabled="true"></button>
             </label>
           </div>
         </div>
       </div>
       <div class="row">
        <div class="col-md-6">
          <div class="form-group">
           <label>Visualização:</label><br>
           <label class="radio-inline">
             <input type="radio" name="visualizacao" value="A" {{ ($modulo->visualizacao == 'A') ? 'checked' : '' }}> Aberto
           </label>
           <label class="radio-inline">
             <input type="radio" name="visualizacao" value="F" {{ ($modulo->visualizacao == 'F') ? 'checked' : '' }}> Fechado
           </label>
         </div>
       </div>
     </div>
     <div class="row">
      <div class="col-md-6">
        <div class="form-group">
         <label>Status:</label><br>
         <label class="radio-inline">
          <input type="radio" name="sts" value="A" {{ ($modulo->sts == 'A') ? 'checked' : '' }}> Ativo
        </label>
        <label class="radio-inline">
          <input type="radio" name="sts" value="I" {{ ($modulo->sts == 'I') ? 'checked' : '' }}> Inativo
        </label>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-6">
     <div class="form-group pull-right">
       <button type="submit" class="btn btn-success">Editar</button>
       <a href="{{url("/modulos/$disciplina")}}"><button type="button" class="btn btn-danger">Cancelar</button> </a>
     </div>
   </div>
 </div>
</form>

</div>
</div>
</div>
</div>
@endsection