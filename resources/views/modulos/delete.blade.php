@extends("layouts.app")

@section("content")
<div class="row" style="margin-left: 20px;">

	<div class="col-md-5 well" style="height: 103px;">
		<div class="col-md-12">
			<h4>Deseja excluir essa Módulo?</h4>
			<div style="float:right; margin-top: -35px">
				<a class="btn btn-default" href="{{url("modulos/$disciplina")}}">
					<i class="ion-android-arrow-back"></i>&nbsp;Cancelar
				</a>
				<a class="btn btn-danger" href="{{url("modulos/$disciplina/destroy/$modulo->id")}}"">
					<i class="ion-ios-trash"></i>&nbsp;Excluir
				</a>
			</div>
		</div>
	</div>

	<div class="col-md-5">
		<div class="panel panel-default">
			<div class="panel-heading">
				Módulo
			</div>
			
			<div class="panel-body">
				<p>{{$modulo->nome}}</p>
			</div>
		</div>
	</div>
</div>


</div>
@endsection