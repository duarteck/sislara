@extends("template.app")

@section("content")


    <div class="col-sm-12" style="padding-bottom: 10px">
            @foreach(range('A', 'Z') as $letra)
            <div class="btn-group">
               <p>{{ $letra }}</p>
            </div>
        @endforeach
    </div>


    @foreach($pessoas as $pessoa)
        <div class="col-md-3">
            <div class="panel panel-info">
                <div class="panel-heading">
                    {{ $pessoa->nome }}
                </div>
                <div class="panel-body">
                    @foreach($pessoa->telefones as $telefone)
                        <p><strong>Telefone: </strong> ({{ $telefone->ddd }}) {{ $telefone->telefone }}</p>
                    @endforeach
                </div>
            </div>
        </div>
    @endforeach
@endsection

