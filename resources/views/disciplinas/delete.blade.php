@extends("layouts.app")

@section("content")
<div class="container">
	<div class="row">
		<div class="col-md-7" style="height: 103px;background: rgb(221,221,221);padding: 15px 10px; border-radius: 3px;margin-bottom: 15px;">
			<div class="col-md-7 col-sm-7">
				<h4>Deseja excluir essa disciplina?</h4>
			</div>
			
			<div class="col-md-5 col-sm-5">
				<a class="btn btn-default" href="{{url("disciplinas")}}">
					<i class="ion-android-arrow-back"></i>&nbsp;Cancelar
				</a>
				<a class="btn btn-danger" href="{{url("disciplinas/$disciplina->id/destroy")}}">
					<i class="ion-ios-trash"></i>&nbsp;Excluir
				</a>
			</div>
		</div>
	
		<div class="col-md-5">
			<div class="panel panel-default">
				<div class="panel-heading">
					Disciplina
				</div>
			
				<div class="panel-body">
					<p>{{$disciplina->nome}}</p>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection