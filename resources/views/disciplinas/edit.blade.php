@extends("layouts.app") @section("content")

<div id="page-wrapper">
	<div class="container" style="max-width: 900px;">
		<div class="panel panel-default">
			<div class="panel-heading">Editar de Disciplina</div>
			<br> <br>

			<div class="panel-body">

				<form action="{{url('/disciplinas/update')}}" method="POST">
					{{csrf_field()}} <input type="hidden" name="id"
						value="{{ $disciplina->id }}">
					<div class="row">
						<div class="col-md-10">
							<div class="form-group">
								<label>Nome da Disciplina:</label> <input type="text"
									class="form-control" name="nome"
									value="{{ $disciplina->nome }}" placeholder="Disciplina"
									disabled="disabled">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-2">
							<div class="form-group">
								<label style="white-space: nowrap;">Id do Curso:</label> <input type="text"
									class="form-control" name="cursoid"
									value="{{ $disciplina->cursoid }}" placeholder="Curso">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-10">
							<div class="form-group">
								<label>Nome do Professor:</label> <input type="text"
									class="form-control" name="professor"
									value="{{ $disciplina->professor }}" placeholder="Professor">
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label>Data Início:</label>
								<div class="input-group date">
									<span class="input-group-addon"><i class="ion-calendar"></i></span>
									<input name="dtIn" value="{{ \Carbon\Carbon::parse($disciplina->dtIn)->format('d/m/Y') }}" type="text"
										class="form-control mask-date datepicker" placeholder="dd/mm/aaaa"
										required="required">
								</div>
							</div>
						</div>

						<div class="col-md-3">
							<div class="form-group">
								<label>Data Final:</label>
								<div class="input-group date">
									<span class="input-group-addon"><i class="ion-calendar"></i></span>
									<input name="dtFm" type="text" class="form-control mask-date datepicker"
										value="{{ \Carbon\Carbon::parse($disciplina->dtFm)->format('d/m/Y') }}" placeholder="dd/mm/aaaa"
										required="required">
								</div>
							</div>
						</div>
					</div>

                    <div class="row">
                        <div class="col-md-10">
                            <div class="form-group">
                               <img src="{{ url("storage/topos/{$disciplina->topo}") }}" alt="{{ $disciplina->topo }}" class="img-responsive">
 	                        </div>
					    </div>
                    </div>
                    
                    <div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="exampleInputFile">Adicionar Topo da Sala:</label> <input
									type="file" id="image" name="image">
								<p class="help-block">O topo terá que ter as seguintes dimensões
									720x325.</p>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Status:</label><br> <label class="radio-inline"> <input
									type="radio" name="sts" value="A" {{ ($disciplina->sts == 'A')
									? 'checked' : '' }}> Ativo
								</label> <label class="radio-inline"> <input type="radio"
									name="sts" value="I" {{ ($disciplina->sts == 'I') ? 'checked' :
									'' }}> Inativo
								</label>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group pull-right">
								<a href="{{url("/disciplinas")}}"><button type="button"
										class="btn btn-danger">Cancelar</button> </a>
								<button type="submit" class="btn btn-success">Salvar</button>
							</div>
						</div>
					</div>
				</form>

			</div>
		</div>
	</div>
</div>

@endsection
