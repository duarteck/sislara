@extends("layouts.app") @section("content")

<div id="page-wrapper">
	<div class="row">
		<div class="col-md-12">
			<ul class="breadcrumb" style="margin-right: 0px;">
				<li class="completed"><a href="#"><i
						style="margin-left: 0px; font-size: 20px" class="ion-android-menu"></i></a></li>
				<li class="active"><a href="#"><i
						style="margin-right: 15px; font-size: 20px"
						class="ion-android-home"></i>Página Inicial</a></li>
				<li class="active">
					<a href="{{url("/disciplinas")}}">
						<i style="margin-right: 15px; font-size: 20px" class="ion-university"></i>
						Listagem de Disciplinas
					</a>
				</li>
			</ul>
		</div>
	</div>

	<div class="col-md-12">
		<div class="panel panel-default">
			<a href="{{url("/disciplinas/novo")}}">
				<button type="submit" style="margin-top: 5px; margin-right: 5px;"
					class="btn btn-default btn-sm pull-right">
					<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
				</button>
			</a>
			<div class="panel-heading">Listagem de Disciplinas</div>
			<br> <br>

			<div class="panel-body">

				@if (session('message'))
				<div class="alert alert-success fade in">
					<a href="#" class="close" data-dismiss="alert" aria-label="close"
						title="close">×</a>{{ session('message') }}
				</div>
				@endif

				<table id="example" class="table table-striped table-responsive" cellspacing="0"
					width="100%">
					<thead>
						<tr>
							<th>#</th>
							<th>Nome da Disciplina</th>
						</tr>
					</thead>
					<tbody>
						@foreach($disciplinas as $disciplina)
						<tr>
							<td>{{$disciplina->id}}</td>
							<td>{{$disciplina->nome}}
								<div class="btn-group pull-right">
									<button type="button" class="btn btn-default dropdown-toggle"
										data-toggle="dropdown" aria-haspopup="true"
										aria-expanded="false">
										<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
										<span class="caret"></span>
									</button>
									<ul class="dropdown-menu">
										<li><a href="#" class="small-box-footer" data-toggle="modal"
											data-target="#myModalap{{$disciplina->id}}"><span
												class="glyphicon glyphicon-bullhorn" aria-hidden="true"></span>
												Apresentação</a></li>
										<li><a href="{{url("/wscourse/$disciplina->token")}}"
												target="_blank"><span class="ion-eye" aria-hidden="true"></span>
												Pré-Visualizar
										</a></li>
										<li><a href="#" class="small-box-footer" data-toggle="modal"
											data-target="#myModal1"><span class="ion-android-share-alt"
												aria-hidden="true"></span> Incorporar</a></li>
										<li><a href="{{url("/disciplinas/$disciplina->id/editar")}}"><span
												class="glyphicon glyphicon-edit" aria-hidden="true"></span>
												Editar Disciplina
										</a></li>
										<li role="separator" class="divider"></li>
										<li><a href="{{url("/disciplinas/$disciplina->id/excluir")}}"><span
												class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>
												Excluir
										</a></li>
									</ul>
								</div>
							</td>
						</tr>

						<!-- Modal apre -->
						<div class="modal fade" id="myModalap{{$disciplina->id}}"
							tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header" style="background-color: #00ADD8">
										<h4 class="modal-title" id="myModalLabel" style="color: white">
											<i class="fa fa-code"
												style="margin-right: 15px; font-size: 20px"></i>Apresentação
											da Disciplina
										</h4>
									</div>
									<div class="modal-body">

										<textarea style="margin: 0px; width: 563px; height: 121px;"></textarea>

										<div class="modal-footer">
											<button type="button" class="btn btn-danger"
												data-dismiss="modal">Fechar</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--fim  modal apre-->

						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>




@if(count($disciplinas) > 0)

<!-- Modal cad -->
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header" style="background-color: #00ADD8">
				<h4 class="modal-title" id="myModalLabel" style="color: white">
					<i class="fa fa-code" style="margin-right: 15px; font-size: 20px"></i>Incorporar
					ao Moodle
				</h4>
			</div>
			<div class="modal-body">

				<textarea style="margin: 0px; width: 563px; height: 90px;"><iframe src="{{url("/wscourse/$disciplina->token")}}" name="slide" marginwidth="0" marginheight="0" scrolling="true" id="slide" align="middle" frameborder="no" height="1300" width="100%"></iframe>
			</textarea>

				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>
</div>
<!--fim  modal acad-->

@endif @endsection
