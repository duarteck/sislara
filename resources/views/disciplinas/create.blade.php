@extends("layouts.app")

@section("content")
<div id="page-wrapper">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        Adicionar de Disciplina
      </div>
      <br><br>
      
      <div class="panel-body">
        @if (session('message'))
          <div class="alert alert-danger fade in">
            <a href="#" class="close" data-dismiss="alert" aria-label="close"
              title="close">×</a>{{ session('message') }}
          </div>
        @endif

        @if (count($courses) == 0)
          <div class="alert alert-danger fade in">
            <a href="#" class="close" data-dismiss="alert" aria-label="close"
              title="close">×</a>Para continuar é preciso preencher os dados de Token e URL no menu 
              <a style="color: red" href="{{url('/configuracoes/'.Auth::user()->id)}}">configurações</a>.
          </div>
        @endif

        <form action="{{url('/disciplinas/store')}}" method="POST" enctype="multipart/form-data">
          {{csrf_field()}}
          <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
          <div class="row">
            <div class="col-md-1">
              <div class="form-group">
                <label>Curso:</label>
                <select name="cursoid" class="form-control" required>
                  <option value=0>Selecione</option>
                  @foreach ($courses as $course)
                      <option value="{{ $course->id }}">{{ $course->shortname }}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Nome do Professor:</label>
                <input type="text" class="form-control" name="professor" placeholder="Professor" required>
              </div>
            </div>
          </div>
          
          <div class="row">
            <div class="col-md-3">                                
              <div class="form-group">
                <label>Data Início:</label>
                <div class="input-group date">
                  <span class="input-group-addon"><i class="ion-calendar"></i></span>
                  <input name="dtIn" type="text" class="form-control mask-date datepicker" placeholder="dd/mm/aaaa"  required="required">
                </div>
              </div>
            </div>

            <div class="col-md-3">                                
              <div class="form-group">
                <label>Data Final:</label>
                <div class="input-group date">
                 <span class="input-group-addon"><i class="ion-calendar"></i></span>
                 <input name="dtFm" type="text" class="form-control mask-date datepicker" placeholder="dd/mm/aaaa"  required="required">
               </div>
             </div>
           </div>
         </div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="exampleInputFile">Adicionar Topo da Sala:</label>
                <img id="img_preview_topo" style="display: block;">     
                <input type="file" id="image" name="image" onchange="viewImgTopo(this)">
								<p class="help-block">O topo terá que ter as seguintes dimensões
									1280x221.</p>
							</div>
						</div>
					</div>

					<div class="row">
          <div class="col-md-6">
            <div class="form-group">
             <label>Status:</label><br>
             <label class="radio-inline">
              <input type="radio" name="sts" value="A" checked> Ativo
            </label>
            <label class="radio-inline">
              <input type="radio" name="sts" value="I"> Inativo
            </label>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
         <div class="form-group pull-right">  
           <a href="{{url("/disciplinas")}}"><button type="button" class="btn btn-danger">Cancelar</button> </a>
           <button type="submit" class="btn btn-success">Adicionar</button>
         </div>
       </div>
     </div>
   </form>

 </div>
</div>
</div>
</div>

<script type="text/javascript">
  function viewImgTopo(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();  
      reader.onload = function (e) {
        var img = new Image;
        img.onload = function() {
          if(img.width == 1280 && img.height == 221 ){
            $('#img_preview_topo').attr('src', e.target.result);
          }else{
            alert("Imagem fora dos padrões de altura e largura!");
          }
        }
        img.src = reader.result;
        };
        reader.readAsDataURL(input.files[0]);
    }
  }
</script>
@endsection