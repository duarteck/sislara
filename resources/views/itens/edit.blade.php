@extends("layouts.app")

@section("content")


<div id="page-wrapper">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        Editar Item
      </div>
      <br><br>

      <div class="panel-body">

        <form action="{{url('/itens/update')}}" method="POST">
          {{csrf_field()}}
          <input type="hidden" name="bloco_id" value="{{$bloco}}">
          <input type="hidden" name="id" value="{{ $item->id }}">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Nome da Modulo:</label>
                <input type="text" class="form-control" name="nome" value="{{$item->nome}}" placeholder="Nome">
              </div>
            </div>
          </div>

  <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Link do Item:</label>
                <input type="text" class="form-control" name="link" value="{{$item->link}}" placeholder="Link">
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group">

  <label class="radio-inline">
                            <input type="radio" name="icone" id="icone" value="ion-document-text" {{ ($item->icone == 'ion-document-text') ? 'checked' : '' }}> <span class="ion-document-text"></span>
                        </label>
                        <label class="radio-inline">
                           <input type="radio" name="icone" id="icon" value="glyphicon glyphicon-play" {{ ($item->icone == 'glyphicon glyphicon-play') ? 'checked' : '' }}> <span class="glyphicon glyphicon-play"></span>
                        </label>
                        <label class="radio-inline">
                           <input type="radio" name="icone" id="icone" value="glyphicon glyphicon-globe" {{ ($item->icone == 'glyphicon glyphicon-globe') ? 'checked' : '' }}> <span class="glyphicon glyphicon-globe"></span>
                        </label>
                        <label class="radio-inline">
                           <input type="radio" name="icone" id="icone" value="glyphicon glyphicon-folder-open" {{ ($item->icone == 'glyphicon glyphicon-folder-open') ? 'checked' : '' }}> <span class="glyphicon glyphicon-folder-open"></span>
                        </label>
                          <label class="radio-inline">
                           <input type="radio" name="icone" id="icone" value="ion-android-textsms" {{ ($item->icone == 'ion-android-textsms') ? 'checked' : '' }}> <span class="ion-android-textsms"></span>
                        </label>
                          <label class="radio-inline">
                           <input type="radio" name="icone" id="icone" value="ion-android-create" {{ ($item->icone == 'ion-android-create') ? 'checked' : '' }}> <span class="ion-android-create"></span>
                        </label>

  </div>
         </div>
       </div>

     <div class="row">
      <div class="col-md-6">
        <div class="form-group">
         <label>Status:</label><br>
         <label class="radio-inline">
          <input type="radio" name="sts" value="A" {{ ($item->sts == 'A') ? 'checked' : '' }}> Ativo
        </label>
        <label class="radio-inline">
          <input type="radio" name="sts" value="I" {{ ($item->sts == 'I') ? 'checked' : '' }}> Inativo
        </label>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-6">
     <div class="form-group pull-right">
       <button type="submit" class="btn btn-success">Editar</button>
       <a href="{{url("/itens/$bloco")}}"><button type="button" class="btn btn-danger">Cancelar</button> </a>
     </div>
   </div>
 </div>
</form>

</div>
</div>
</div>
</div>
@endsection