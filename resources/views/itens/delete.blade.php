@extends("layouts.app")

@section("content")
<div class="row" style="margin-left: 20px;">

    <div class="col-md-5 well" style="height: 103px;">
        <div class="col-md-12">
            <h4>Deseja excluir esse Item?</h4>
            <div style="float:right; margin-top: -35px">
                <a class="btn btn-default" href="{{url("itens/$bloco")}}">
                    <i class="ion-android-arrow-back"></i>&nbsp;Cancelar
                </a>
                <a class="btn btn-danger" href="{{url("itens/$bloco/destroy/$item->id")}}"">
                    <i class="ion-ios-trash"></i>&nbsp;Excluir
                </a>
            </div>
        </div>
    </div>

    <div class="col-md-5">
        <div class="panel panel-default">
            <div class="panel-heading">
                Item
            </div>

            <div class="panel-body">
                <p>{{$item->nome}}</p>
            </div>
        </div>
    </div>
</div>


</div>
@endsection