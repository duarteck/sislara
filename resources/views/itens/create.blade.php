@extends("layouts.app")

@section("content")

<div id="page-wrapper">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        Cadastrar Item
      </div>
      <br><br>

      <div class="panel-body">

        <form action="{{url('/itens/store')}}" method="POST">
          {{csrf_field()}}
          <input type="hidden" name="bloco_id" value="{{ $bloco }}">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Nome do Item:</label>
                <input type="text" class="form-control" name="nome" placeholder="Nome">
              </div>
            </div>
          </div>
         <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Link do Item:</label>
                <input type="text" class="form-control" name="link" placeholder="Link">
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Icone do Item:</label><br>
                        <label class="radio-inline">
                            <input type="radio" name="icone" id="icone" value="ion-document-text"> <span class="ion-document-text"></span>
                        </label>
                        <label class="radio-inline">
                           <input type="radio" name="icone" id="icon" value="glyphicon glyphicon-play"> <span class="glyphicon glyphicon-play"></span>
                        </label>
                        <label class="radio-inline">
                           <input type="radio" name="icone" id="icone" value="glyphicon glyphicon-globe"> <span class="glyphicon glyphicon-globe"></span>
                        </label>
                        <label class="radio-inline">
                           <input type="radio" name="icone" id="icone" value="glyphicon glyphicon-folder-open"> <span class="glyphicon glyphicon-folder-open"></span>
                        </label>
                          <label class="radio-inline">
                           <input type="radio" name="icone" id="icone" value="ion-android-textsms"> <span class="ion-android-textsms"></span>
                        </label>
                          <label class="radio-inline">
                           <input type="radio" name="icone" id="icone" value="ion-android-create"> <span class="ion-android-create"></span>
                        </label>
           </div>
         </div>
       </div>

     <div class="row">
      <div class="col-md-6">
        <div class="form-group">
         <label>Status:</label><br>
         <label class="radio-inline">
          <input type="radio" name="sts" value="A"> Ativo
        </label>
        <label class="radio-inline">
          <input type="radio" name="sts" value="I"> Inativo
        </label>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-6">
     <div class="form-group pull-right">
       <button type="submit" class="btn btn-success">Adicionar</button>
       <a href="{{url("/itens/$bloco")}}"><button type="button" class="btn btn-danger">Cancelar</button> </a>
     </div>
   </div>
 </div>
</form>

</div>
</div>
</div>
</div>
@endsection