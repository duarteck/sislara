	<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Sala Moodle</title>
<meta
	content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
	name='viewport'>
	
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	
	<!-- Ionicons -->
    <link href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
<!-- bootstrap 3.0.2 -->
<link href="http://eadbooks.tempsite.ws/hadron/seav/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- font Awesome -->
<link href="http://eadbooks.tempsite.ws/hadron/seav/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Ionicons -->
<link href="http://eadbooks.tempsite.ws/hadron/seav/css/ionicons.min.css" rel="stylesheet" type="text/css" />
<!-- Morris chart -->
<link href="http://eadbooks.tempsite.ws/hadron/seav/css/morris/morris.css" rel="stylesheet" type="text/css" />
<!-- jvectormap -->
<link href="http://eadbooks.tempsite.ws/hadron/seav/css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet"
	type="text/css" />
<!-- fullCalendar -->
<link href="http://eadbooks.tempsite.ws/hadron/seav/css/fullcalendar/fullcalendar.css" rel="stylesheet"
	type="text/css" />
<!-- Daterange picker -->
<link href="http://eadbooks.tempsite.ws/hadron/seav/css/daterangepicker/daterangepicker-bs3.css"
	rel="stylesheet" type="text/css" />
<!-- bootstrap wysihtml5 - text editor -->
<link href="http://eadbooks.tempsite.ws/hadron/seav/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"
	rel="stylesheet" type="text/css" />
<!-- Theme style -->
<link href="http://eadbooks.tempsite.ws/hadron/seav/css/AdminLTE.css" rel="stylesheet" type="text/css" />

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

<style>
body {
	background-color: #FFFFFF
}

.corpo_topo {
	height: 241px;
	width: 100%;
	display: inline-block;
	background: url({{ asset('img/bg.png')}});
}
.blocoE {
	color: #FFFFFF;
	float: left;
	display: inline-block;
	position: relative;
	z-index: 1;
	height: 106px;
	width: 275px;
}

.header_1 {
	color: #fff;
	font-weight:bold;
}

.header_2 {
	color: #fff;
}

h2 {
	margin: 0;
	padding: 0;
	margin-left: 10px;
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
	font-size: 20px;
	padding-top: 1px;
	padding-left: 1px;
	padding-bottom: 8px;
}

.blocoE_txt {
	margin-top: 15px;
}

.blocoD ul {
	border: #E8E8E9 1px solid;
	width: 97px;
	box-shadow: 2px 1px 10px #858585;
}

.blocoD ul li {
	background: #FFF;
	color: #575757;
	padding-top: 6px;
	padding-bottom: 6px;
	padding-left: 10px;
	cursor: pointer;
}

.blocoD ul li:hover {
	background: #f1561e;
	color: #fff;
}

.span_carret {
	display: inline-block;
	width: 21px;
	height: 18px;
	background: url(images/carret.png) no-repeat;
	margin-left: 65px;
	margin-bottom: -15px;
}

.drop_nav {
	display: inline-block;
	right: 0;
	position: absolute;
	margin-top: -15px;
	display: none;
	overflow: hidden;
}

.corpo_topo a {
	text-decoration: none;
	color: #fff;
}
</style>

<!--menu novo-->
<style>
.breadcrumb {
	padding: 0px;
	background: #00A65A;
	list-style: none;
	overflow: hidden;
	margin-top: -5px;
}

.breadcrumb>li+li:before {
	padding: 0;
}

.breadcrumb li {
	float: left;
}

.breadcrumb li.active a {
	background: brown; /* fallback color */
	background: #00A65A;
}

.breadcrumb li.completed a {
	background: brown; /* fallback color */
	background: #00A65A;
}

.breadcrumb li.active a:after {
	border-left: 30px solid #00A65A;
}

.breadcrumb li.completed a:after {
	border-left: 30px solid #00A65A;
}

.breadcrumb li a {
	color: white;
	text-decoration: none;
	padding: 10px 0 10px 45px;
	position: relative;
	display: block;
	float: left;
}

.breadcrumb li a:after {
	content: " ";
	display: block;
	width: 0;
	height: 0;
	border-top: 50px solid transparent;
	/* Go big on the size, and let overflow hide */
	border-bottom: 50px solid transparent;
	border-left: 30px solid #00A65A;
	position: absolute;
	top: 50%;
	margin-top: -50px;
	left: 100%;
	z-index: 2;
}

.breadcrumb li a:before {
	content: " ";
	display: block;
	width: 0;
	height: 0;
	border-top: 50px solid transparent;
	/* Go big on the size, and let overflow hide */
	border-bottom: 50px solid transparent;
	border-left: 30px solid white;
	position: absolute;
	top: 50%;
	margin-top: -50px;
	margin-left: 1px;
	left: 100%;
	z-index: 1;
}

.breadcrumb li:first-child a {
	padding-left: 15px;
}

.breadcrumb li a:hover {
	background: #00A65A
}

.breadcrumb li a:hover:after {
	border-left-color: #00A65A;
	!
	important;
}
</style>

<!--fim menu novo-->

</head>
<body class="skin-blue">


	<!--topo com as informações do curso como nome da disciplina e data-->
	<div class="corpo_topo">
		<div class="blocoE" style="margin-left: 70px; margin-top: 50px;">
			<div class="blocoE_txt">
				<h3>
					<span class="header_1">{{ $disciplina->nome }}</span>
				</h3>
				<h5 class="header_2">Prof. {{ $disciplina->professor }}</h5>
				<h5 class="header_2">Período: {{ $disciplina->dtIn }} a {{ $disciplina->dtFm }}</h5>
			</div>
		</div>
		<div class="blocoD"></div>
	</div>


	<!--novo menu de orientaçao-->
	<div class="">
		<div class="row">
			<ul class="breadcrumb">
				<li class="completed"><a href="#"><i
						style="margin-left: 15px; font-size: 20px"
						class="ion-android-menu"></i></a></li>
				<li class="active"><a href="http://ava.ifma.edu.br/" target="_blank"><i
						style="margin-right: 15px; font-size: 20px"
						class="ion-android-home"></i>Página Inicial</a></li>
				<li class="active"><a href="#"><i
						style="margin-right: 15px; font-size: 20px" class="ion-university"></i>Meus
						Cursos</a></li>
						<li class="active"><a href="#"><i
						style="margin-right: 15px; font-size: 20px"
						class="ion-ios-calendar-outline"></i>Calendário</a></li>
				<li class="active"><a href="#"><i
						style="margin-right: 15px; font-size: 20px"
						class="ion-ios-navigate-outline"></i>Guia do Aluno</a></li>
				<li class="active"><a href="arquivos_download/modelo_sala.rar"><i
						style="margin-right: 15px; font-size: 20px"
						class="ion-ios-cloud-download-outline"></i>Download da Sala</a></li>
			</ul>
		</div>
	</div>


	<!--fim novo menu de orientaçao-->

	<!-- menu de orientação -->
	<!--<div style="text-align:right;">-->
	<!--<div class="btn-group">-->
	<!--<button type="button" class="btn btn-primary">Menu de Orienta&ccedil;&otilde;es</button>-->
	<!--<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"> <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> </button>-->
	<!--<ul class="dropdown-menu" role="menu">-->
	<!--<li><a href="#"><i class="fa fa-pencil-square-o"></i> Guia do Aluno</a></li>-->
	<!--<li><a href="#" target="_blank"><i class="fa fa-calendar"></i> Cronograma</a></li>-->
	<!--<li><a href="#" target="_blank"><i class="fa fa-quote-left"></i> Intera&ccedil;&atilde;o</a></li>-->
	<!--<li><a href="#" target="_blank"><i class="fa fa-location-arrow"></i> Avisos</a></li>-->
	<!--<li class="divider"></li>-->
	<!--<li><a href="#" target="_blank"><i class="fa fa-laptop"></i> Tutorial - AVA</a></li>-->
	<!--</ul>-->
	<!--</div>-->
	<!--</div>-->
	<!--fim menu de orientação-->

	<br>

	<!--entrada dos blocos superiores-->


	<!--Bloco de apresentacao-->
	<div class="row">
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-aqua">
				<div class="inner">
					<h3>Apresenta&ccedil;&atilde;o</h3>
					<p>Apresenta&ccedil;&atilde;o da disciplina</p>
				</div>
				<div class="icon">
					<i class="ion-ios-book-outline"></i>
				</div>
				<a href="#" class="small-box-footer" data-toggle="modal"
					data-target="#myModal"> Visualizar <i
					class="ion-log-in"></i>
				</a>
			</div>
		</div>
		<!-- ./col -->


		<!--Modal de Apresentação da Disciplina-->
		<!-- Modal -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header" style="background-color: #00ADD8">
						<!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
						<h4 class="modal-title" id="myModalLabel" style="color: white">
							<i class="ion-ios-book-outline"
								style="margin-right: 15px; font-size: 20px"></i>Apresentação da
							Disciplina
						</h4>
					</div>
					<div class="modal-body">
						<!--tabs de Apresentação da Disciplina-->
						<div>
							<!-- Nav tabs -->
							<ul class="nav nav-tabs" role="tablist">
								<li role="presentation" class="active"><a href="#home"
									aria-controls="home" role="tab" data-toggle="tab"><i
										class="ion-android-happy"
										style="margin-right: 15px; font-size: 20px"></i>Saudações</a></li>
								<li role="presentation"><a href="#fasciculo"
									aria-controls="profile" role="tab" data-toggle="tab"><i
										class="ion-android-document"
										style="margin-right: 15px; font-size: 20px"></i>Fascículo da
										Disciplina</a></li>
								<li role="presentation"><a href="#profile"
									aria-controls="profile" role="tab" data-toggle="tab"><i
										class="ion-social-youtube"
										style="margin-right: 15px; font-size: 20px"></i>Vídeo de
										Apresentação</a></li>
							</ul>
							<!-- Tab panes -->
							<div class="tab-content">
								<div role="tabpanel" class="tab-pane active" id="home"
									style="text-align: justify">
									<br>
									<!--aqui entra o texto  motivacional na guia de saudações-->
									<p>Área do Conteúdo</p>
								</div>

								<div role="tabpanel" class="tab-pane" id="fasciculo">
									<br>
									<!--aqui entra o link dos fascúclos na guia de fasciculos-->
									<p>Área do Fasciculo</p>
								</div>
								<div role="tabpanel" class="tab-pane" id="profile">
									<br>
									<!--aqui entra o link do vídeo  na guia de Vídeos-->
									<p>Área do Vídeo</p>
								</div>
							</div>
						</div>
						<!--fim tabs-->
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
					</div>
				</div>
			</div>
		</div>
		<!--fim  modal-->
		<!--fim bloco de apresentacao  -->



		<!--bloco dos planos de ensino  -->
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-yellow">
				<div class="inner">
					<h3>Planos</h3>
					<p>Planos da Disciplina</p>
				</div>
				<div class="icon">
					<i class="ion-ios-paper-outline"></i>
				</div>
				<a href="#" class="small-box-footer"> Visualizar <i
					class="ion-log-in"></i>
				</a>
			</div>
		</div>
		<!--fim dos blocos planos de ensino  -->


		<!--bloco de Participantes  -->
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-purple">
				<div class="inner">
					<h3>Participantes</h3>
					<p>Participantes da Disciplina</p>
				</div>
				<div class="icon">
					<i class="ion-android-contacts"></i>
				</div>
				<a href="#" class="small-box-footer"> Visualizar <i
					class="ion-log-in"></i>
				</a>
			</div>
		</div>
		<!-- fim bloco de Participantes  -->


		<!--bloco de notas  -->
		<div class="col-lg-3 col-xs-6">
			<div class="small-box bg-red">
				<div class="inner">
					<h3>Notas</h3>
					<p>Notas da Disciplina</p>
				</div>
				<div class="icon">
					<i class="ion-ios-calculator-outline"></i>
				</div>
				<a href="#" class="small-box-footer"> Visualizar <i
					class="ion-log-in"></i>
				</a>
			</div>
		</div>
	</div>
	<!--fim bloco de notas  -->


	@foreach($disciplina->modulos as $modulo)
	<div class="row">
		<div class="box box-solid">
			<div class="box-body">
				<div class="box-group" id="accordion">
					<div class="panel box box-{{ $modulo->cor }}">
						<div class="box-header">
							<h4 class="box-title">
								<a data-toggle="collapse" data-parent="#accordion"
									href="#collapse{{ $modulo->id }}"> <i class="ion-ios-paperplane"></i>
									{{ $modulo->nome }}
								</a>
							</h4>
						</div>

						<div id="collapse{{ $modulo->id }}" class="panel-collapse collapse in">
							<div class="box-body">
								<div class="row">
									@foreach($modulo->blocos as $bloco)
									<div class="col-md-4">
									
									   <!-- Primary box -->
										<div class="box box-solid box-{{ $modulo->cor }}">
											<div class="box-header">
												<h3 class="box-title">
													<i class="{{ $bloco->icone }}"></i> {{ $bloco->nome }}
												</h3>
												<div class="box-tools pull-right">
													<button class="btn btn-{{ $modulo->cor }} btn-sm"
														data-widget="collapse">
														<i class="ion-minus"></i>
													</button>
												</div>
											</div>
                                            <div class="box-body">
                                                @foreach($bloco->itens as $item)
                                                <p>
											        <a href="{{ $item->link }}" target="_blank" id="button1" data-sample-id="{{ $item->id }}" onclick="myFunction()"> 
													    <span class="{{ $item->icone }}"></span>
														<strong>{{ $item->nome }}</strong>
													</a>
												</p>
												@endforeach
											</div>
										</div>
										<!-- Fim Primary box -->
									
									</div>
								    @endforeach
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
	@endforeach

	<!-- jQuery 2.0.2 -->
	<script src="http://eadbooks.com.br/hadron/seav/js/1jquery.min.js"></script>
	<!-- jQuery UI 1.10.3 -->
	<script src="http://eadbooks.com.br/hadron/seav/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
	<!-- Bootstrap -->
	<script src="http://eadbooks.com.br/hadron/seav/js/bootstrap.min.js" type="text/javascript"></script>
	<!-- Morris.js charts -->
	<script src="http://eadbooks.com.br/hadron/seav/js/1raphael-min.js"></script>
	<script src="http://eadbooks.com.br/hadron/seav/js/plugins/morris/morris.min.js" type="text/javascript"></script>
	<!-- Sparkline -->
	<script src="http://eadbooks.com.br/hadron/seav/js/plugins/sparkline/jquery.sparkline.min.js"
		type="text/javascript"></script>
	<!-- jvectormap -->
	<script src="http://eadbooks.com.br/hadron/seav/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"
		type="text/javascript"></script>
	<script src="http://eadbooks.com.br/hadron/seav/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"
		type="text/javascript"></script>
	<!-- fullCalendar -->
	<script src="http://eadbooks.com.br/hadron/seav/js/plugins/fullcalendar/fullcalendar.min.js"
		type="text/javascript"></script>
	<!-- jQuery Knob Chart -->
	<script src="http://eadbooks.com.br/hadron/seav/js/plugins/jqueryKnob/jquery.knob.js"
		type="text/javascript"></script>
	<!-- daterangepicker -->
	<script src="http://eadbooks.com.br/hadron/seav/js/plugins/daterangepicker/daterangepicker.js"
		type="text/javascript"></script>
	<!-- Bootstrap WYSIHTML5 -->
	<script
		src="http://eadbooks.com.br/hadron/seav/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"
		type="text/javascript"></script>
	<!-- iCheck -->
	<script src="http://eadbooks.com.br/hadron/seav/js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    <!-- AdminLTE App -->
	<script src="http://eadbooks.com.br/hadron/seav/js/AdminLTE/app.js" type="text/javascript"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
	<script src="http://eadbooks.com.br/hadron/seav/js/AdminLTE/dashboard.js" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
	<script src="http://eadbooks.com.br/hadron/seav/js/AdminLTE/demo.js" type="text/javascript"></script>
	
	
	
	 <script type="text/javascript">

	   $(document).on('click','#button1',function(){
		 var dataId = $(this).data("sample-id");
		// alert(dataId);
		 var _token = $('meta[name="csrf-token"]').attr('content');
		  $.ajax({
	            url: "/geo-info-response",
	            type:'POST',
	            data: {_token:_token, dataId:dataId},
	            success: function(data) {
	                if($.isEmptyObject(data.error)){
	                	alert(data.success);
	                }else{
	                	//printErrorMsg(data.error);
	                	alert(data.error);
	                }
	            }
	        });



		  function printErrorMsg (msg) {
				$(".print-error-msg").find("ul").html('');
				$(".print-error-msg").css('display','block');
				$.each( msg, function( key, value ) {
					$(".print-error-msg").find("ul").append('<li>'+value+'</li>');
				});
			}

	        
		 });  



	 
        function myFunction(){

       var postId = $("#button1").data("sample-id"); // Get the post ID from our data attribute
  		// registerPostClick2(postId); // Pass that ID to our function. 
  		//alert($("#button1").data("sample-id"));
  	   var id = postId;
  		  
          $.ajaxSetup({
              headers: {
            	  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          })
  		 
  		 $.ajax({
        type: "POST",
        url: './geo-info-response',
        data: "",
        success: function() {
            console.log("Geodata sent");
        }
    });
		  
       //   alert(postId);
}
    


        function registerPostClick2(postId) {
        	
        var id = postId; // A random variable for this example
        $.ajaxSetup({
            headers: {
          	  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        })

        $.ajax({
            method: 'POST', // Type of response and matches what we said in the route
            url: '/customer/ajaxupdate', // This is the url we gave in the route
            data: {'id' : id}, // a JSON object to send back
            success: function(response){ // What to do if we succeed
                console.log(response); 
            },
            error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                console.log(JSON.stringify(jqXHR));
                console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
            }
        });
        alert(postId);
        
        }

        
	
	function registerPostClick(postId) {
		alert(postId);
		
  $.ajaxSetup({
      headers: {
    	  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  })
  $.ajax({
    type: 'post',
    dataType: 'JSON',
    url: '/post/' + postId + '/' + postId,
    error: function (xhr, ajaxOptions, thrownError) {
           console.log(xhr.status);
           console.log(JSON.stringify(xhr.responseText));
       }
  });
}
	</script>
	
	
	
	<script type="text/javascript">

	

	   
	


</script>
	
	
	
	
	
	
</body>
</html>