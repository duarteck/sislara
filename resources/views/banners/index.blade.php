@extends("layouts.app")

@section("content")


<div id="page-wrapper">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        Inserir de Banner
      </div>
      <br><br>

      <div class="panel-body">

        <form action="{{url('/banners/store')}}" method="POST">
          {{csrf_field()}}
          <input type="hidden" name="disciplina_id" value="{{ $disciplina }}">

          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Saudação da Disciplina:</label>
                <textarea style="margin: 0px;  height: 130px;" class="form-control" >
                </textarea>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Fascículo da Disciplina:</label>
                <input type="text" class="form-control" name="fasciculo" placeholder="Link">
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                <label>Vídeo da Disciplina:</label>
                <input type="text" class="form-control" name="video" placeholder="Link">
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Plano da Disciplina:</label>
                <input type="text" class="form-control" name="planos" placeholder="Link">
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                <label>Participantes da Disciplina:</label>
                <input type="text" class="form-control" name="participantes" placeholder="Link">
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Vídeo da Disciplina:</label>
                <input type="text" class="form-control" name="video" placeholder="Link">
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                <label>Notas da Disciplina:</label>
                <input type="text" class="form-control" name="notas" placeholder="Link">
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
             <div class="form-group pull-right">
               <button type="submit" class="btn btn-success">Adicionar</button>
               <a href="{{url("/disciplinas")}}"><button type="button" class="btn btn-danger">Cancelar</button> </a>
             </div>
           </div>
         </div>
       </form>

     </div>
   </div>
 </div>
</div>
@endsection