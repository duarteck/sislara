<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Sistemas Moodle.">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Sislara</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Custom Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <link href='//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>

    <style>
    .breadcrumb {
        padding: 0px;
        background: #F8F8F8;
        list-style: none;
        overflow: hidden;
        margin-top: -15px;
        font-style: italic;
    }
    .breadcrumb>li+li:before {
        padding: 0;
    }
    .breadcrumb li {
        float: left;
    }
    .breadcrumb li.active a {
        background: brown;                   /* fallback color */
        background: #F8F8F8 ;
    }
    .breadcrumb li.completed a {
        background: brown;                   /* fallback color */
        background: #F8F8F8;
    }
    .breadcrumb li.active a:after {
        border-left: 25px solid #F8F8F8 ;
    }
    .breadcrumb li.completed a:after {
        border-left: 25px solid #F8F8F8;
    }

    .breadcrumb li a {
        color: #000000;
        text-decoration: none;
        padding: 10px 0 10px 45px;
        position: relative;
        display: block;
        float: left;
    }
    .breadcrumb li a:after {
        content: " ";
        display: block;
        width: 0;
        height: 0;
        border-top: 50px solid transparent;           /* Go big on the size, and let overflow hide */
        border-bottom: 50px solid transparent;
        border-left: 30px solid #00A65A;
        position: absolute;
        top: 50%;
        margin-top: -50px;
        left: 100%;
        z-index: 2;
    }
    .breadcrumb li a:before {
        content: " ";
        display: block;
        width: 0;
        height: 0;
        border-top: 50px solid transparent;           /* Go big on the size, and let overflow hide */
        border-bottom: 50px solid transparent;
        border-left: 30px solid white;
        position: absolute;
        top: 50%;
        margin-top: -50px;
        margin-left: 1px;
        left: 100%;
        z-index: 1;
    }
    .breadcrumb li:first-child a {
        padding-left: 15px;
    }
    .breadcrumb li a:hover { background:#F8F8F8 ; text-decoration: underline; }
    .breadcrumb li a:hover:after { border-left-color:#F8F8F8;   !important; }

</style>

</head>
<body>
    <div>
        <nav class="navbar navbar-default">

            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    SEAV
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    &nbsp;
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @guest
                    <li><a href="{{ route('login') }}">Login</a></li>
                    <li><a href="{{ route('register') }}">Registrar</a></li>
                    @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu">
                            <li><a href="{{ url("/perfil/senha") }}"><span class="glyphicon glyphicon-lock"></span> Alterar Senha</a></li>
                            <li>
                                <a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                <span class="glyphicon glyphicon-log-out"></span>
                                Sair
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                    @endguest
                </ul>
            </div>
        </nav>
        
        <div class="container-fluid">

            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumb" style="margin-right: 0px;">
                        <li class="completed"><a href="#"><i
                                style="margin-left: 0px; font-size: 20px" class="ion-android-menu"></i></a></li>
                        <li class="active">
                            <a href="{{url("/admin")}}">
                                <i style="margin-right: 15px; font-size: 20px" class="ion-university"></i>
                                Listagem de Usuários
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        {{-- <a href="#">
                            <button type="submit" style="margin-top: 5px; margin-right: 5px;"
                                class="btn btn-default btn-sm pull-right">
                                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                            </button>
                        </a> --}}
                        <div class="panel-heading">Pessoa Física</div>
                        <br> <br>

                        <div class="panel-body">

                            {{-- @if (session('message'))
                            <div class="alert alert-success fade in">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close"
                                    title="close">×</a>{{ session('message') }}
                            </div>
                            @endif --}}

                            <table id="fisico" class="table table-striped ">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>CPF</th>
                                        <th>Nome</th>
                                        <th>Email</th>
                                        <th>Celular</th>
                                        <th>Bairro</th>
                                        <th>Cidade</th>
                                        <th>Estado</th>
                                        <th>Plano</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $id_f = 1; @endphp
                                    @foreach($users as $user)
                                    @if($user->pessoa == "Física")
                                    <tr style="overflow: scroll;">
                                        <td class="text-nowrap">{{ $id_f }}</td>
                                        <td class="text-nowrap">{{ $user->cpfcnpj }}</td>
                                        <td class="text-nowrap">{{ $user->name }}</td>
                                        <td class="text-nowrap">{{ $user->email }}</td>
                                        <td class="text-nowrap">{{ $user->celular }}</td>
                                        <td class="text-nowrap">{{ $user->bairro }}</td>
                                        <td class="text-nowrap">{{ $user->cidade }}</td>
                                        <td class="text-nowrap">{{ $user->estado }}</td>
                                        <td class="text-nowrap">{{ $user->plano == 1 ? "Free" : "Básico" }}</td>
                                    </tr>
                                    @php $id_f++ @endphp
                                    @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="panel panel-default">
                        {{-- <a href="#">
                            <button type="submit" style="margin-top: 5px; margin-right: 5px;"
                                class="btn btn-default btn-sm pull-right">
                                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                            </button>
                        </a> --}}
                        <div class="panel-heading">Pessoa Jurídica</div>
                        <br> <br>

                        <div class="panel-body">

                            {{-- @if (session('message'))
                            <div class="alert alert-success fade in">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close"
                                    title="close">×</a>{{ session('message') }}
                            </div>
                            @endif --}}
                        
                            <table id="juridico" class="table table-striped table-responsive">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>CNPJ</th>
                                        <th>Nome</th>
                                        <th>Email</th>
                                        <th>Celular</th>
                                        <th>Bairro</th>
                                        <th>Cidade</th>
                                        <th>Estado</th>
                                        <th>Plano</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $id_j = 1; @endphp
                                    @foreach($users as $user)
                                    @if($user->pessoa == "Jurídica")
                                    <tr>
                                        <td class="text-nowrap">{{ $id_j }}</td>
                                        <td class="text-nowrap">{{ $user->cpfcnpj }}</td>
                                        <td class="text-nowrap">{{ $user->name }}</td>
                                        <td class="text-nowrap">{{ $user->email }}</td>
                                        <td class="text-nowrap">{{ $user->celular }}</td>
                                        <td class="text-nowrap">{{ $user->bairro }}</td>
                                        <td class="text-nowrap">{{ $user->cidade }}</td>
                                        <td class="text-nowrap">{{ $user->estado }}</td>
                                        <td class="text-nowrap">{{ $user->plano == 1 ? "Free" : "Básico" }}</td>
                                    </tr>
                                    @php $id_j++ @endphp
                                    @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>



<script src="{{ asset('js/app.js') }}"></script>
<script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        $('#fisico').DataTable({
            responsive: true,
            language:{
                lengthMenu: "Listar _MENU_",
                zeroRecords: "",
                info: "",
                infoEmpty: "",
                infoFiltered: "",
                sSearch: "Pesquisar",
                oPaginate:{
                    sFirst: "Primeira",
                    sLast: "Última",
                    sNext: "Próxima",
                    sPrevious: "Anterior"
                },
                sEmptyTable: "Sem dados na tabela"
            }
            /******************* Tradutor da Tabela ********************/
            /*oLanguage: {
                oAria: {
                    sSortAscending: ": activate to sort column ascending",
                    sSortDescending: ": activate to sort column descending"
                },
                oPaginate: {
                    sFirst: "First",
                    sLast: "Last",
                    sNext: "Next",
                    sPrevious: "Previous"
                },
                sEmptyTable: "No data available in table",
                sInfo: "Showing _START_ to _END_ of _TOTAL_ entries",
                sInfoEmpty: "Showing 0 to 0 of 0 entries",
                sInfoFiltered: "(filtered from _MAX_ total entries)",
                sInfoPostFix: "",
                sDecimal: "",
                sThousands: ",",
                sLengthMenu: "Show _MENU_ entries",
                sLoadingRecords: "Loading...",
                sProcessing: "Processing...",
                sSearch: "Search:",
                sSearchPlaceholder: "",
                sUrl: "",
                sZeroRecords: "No matching records found"
            }*/
        });

        $('#juridico').DataTable({
            responsive: true,
            language:{
                lengthMenu: "Listar _MENU_",
                zeroRecords: "",
                info: "",
                infoEmpty: "",
                infoFiltered: "",
                sSearch: "Pesquisar",
                oPaginate:{
                    sFirst: "Primeira",
                    sLast: "Última",
                    sNext: "Próxima",
                    sPrevious: "Anterior"
                },
                sEmptyTable: "Sem dados na tabela"
            }
            /******************* Tradutor da Tabela ********************/
            /*oLanguage: {
                oAria: {
                    sSortAscending: ": activate to sort column ascending",
                    sSortDescending: ": activate to sort column descending"
                },
                oPaginate: {
                    sFirst: "First",
                    sLast: "Last",
                    sNext: "Next",
                    sPrevious: "Previous"
                },
                sEmptyTable: "No data available in table",
                sInfo: "Showing _START_ to _END_ of _TOTAL_ entries",
                sInfoEmpty: "Showing 0 to 0 of 0 entries",
                sInfoFiltered: "(filtered from _MAX_ total entries)",
                sInfoPostFix: "",
                sDecimal: "",
                sThousands: ",",
                sLengthMenu: "Show _MENU_ entries",
                sLoadingRecords: "Loading...",
                sProcessing: "Processing...",
                sSearch: "Search:",
                sSearchPlaceholder: "",
                sUrl: "",
                sZeroRecords: "No matching records found"
            }*/
        });
    });
</script>
</body>
</html>
