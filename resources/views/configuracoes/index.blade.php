@extends('layouts.app') @section('content')

{{-- <div class="row">
	<div class="col-md-12">
		<ul class="breadcrumb">
			<li class="completed"><a href="#" target="_blank"><i
					style="margin-left: 15px; font-size: 20px" class="ion-android-menu"></i></a></li>
			<li class="active"><a href="{{url("/")}}" target="_self"><i
					style="margin-right: 15px; font-size: 20px"
					class="ion-android-home"></i>Página Inicial</a></li>
			<li class="active"><a href="{{url("/disciplinas")}}"><i
					style="margin-right: 15px; font-size: 20px" class="ion-university"></i>Listagem
					de Disciplinas</a></li>
		</ul>
	</div>
</div> --}}

<div id="page-wrapper">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">Configurações</div>

			<div class="panel-body">

				@if (session('message'))
				<div class="alert alert-success fade in">
					<a href="#" class="close" data-dismiss="alert" aria-label="close"
						title="close">×</a>{{ session('message') }}
				</div>
				@endif

				<form action="{{url('/configuracoes/store')}}" method="POST">
					{{csrf_field()}} @if($configuracoes->count() == 0) <input
						type="hidden" name="id" value="">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Token:</label> <input type="text" class="form-control"
									name="token" id="token" placeholder="Token">
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Url:</label> <input type="text" class="form-control"
									name="url" id="url" placeholder="Url">
							</div>
						</div>
					</div>
					@else @foreach($configuracoes as $configuracao) <input
						type="hidden" name="id" value="{{ $configuracao->id }}">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Token:</label> <input type="text" class="form-control"
									name="token" value="{{$configuracao->token}}"
									placeholder="Token">
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Url:</label> <input type="text" class="form-control"
									name="url" value="{{$configuracao->url}}" placeholder="Url">
							</div>
						</div>
					</div>
					@endforeach @endif
					<div class="row">
						<div class="col-md-6">
							<div class="form-group pull-right">
									<a href="{{url("/disciplinas")}}"><button type="button"
										class="btn btn-danger">Cancelar</button> </a>
								<button type="submit" class="btn btn-success">Salvar</button>
								
							</div>
						</div>
					</div>
				</form>

			</div>

		</div>
	</div>
</div>
@endsection
